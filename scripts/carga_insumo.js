const Uicontainer = document.querySelector('#myCol');
const Uibtn = document.querySelector('#myElement');
const UiTable = document.querySelector('#myTable');
const UisTipo = document.querySelector('#s1');
const UisSubtipo = document.querySelector('#s2');
const UisName = document.querySelector('#s3');
const UisPresentacion = document.querySelector('#s4');
const UiCantidad = document.querySelector('#cant');
const UiPeso = document.querySelector('#peso');
const UiBtnSubmit = document.querySelector('#mySubmit');
const UiNota = document.querySelector('#nota');
const ci = document.querySelector('#ci');
var nro_documento = document.querySelector('#nro_documento');
const proveedor = document.querySelector('#proveedor');
var inventario = document.querySelector('#inventario');
const inventario_ca = document.querySelector('#inventario_ca');
var cant_total = document.getElementById('cant-total');
var peso_total = document.getElementById('peso-total');
var increment = 0;

document.addEventListener('DOMContentLoaded', () => {
  if(UiBtnSubmit)
  UiBtnSubmit.disabled = true;
});

function showAlert(msg, className){
  const div = document.createElement('div');
  div.appendChild(document.createTextNode(msg));
  div.className = className;

  const parent = document.querySelector('.content');
  parent.insertAdjacentElement('beforebegin', div);

  setTimeout(() => {
    div.remove();
  }, 3500);
}

function addEventHandler(){
  if(proveedor && proveedor.selectedOptions[0].value === "0"){
    showAlert('Por favor seleccione un Proveedor', 'a_alert text-center');
  }else if(nro_documento && nro_documento.value == ""){
    showAlert('Por favor indique el número del correlativo','a_alert text-center');
    nro_documento.classList.add('alert-input');
  }else if(nro_documento && nro_documento.value.length < 8){
    showAlert('El correlativo debe tener 8 digitos','a_alert text-center');
    nro_documento.classList.add('alert-input');
  /* }else if(UisTipo.selectedOptions[0].value === '0'|| UisSubtipo.selectedOptions[0].value === '0' || UisName.selectedOptions[0].value === '0'|| UisPresentacion.value === '0' || UiCantidad.value === ''){
		showAlert('Por favor rellene todos los campos!', 'a_alert text-center'); */
	}else if(UisTipo.selectedOptions[0].value === '0'){
		showAlert('Por favor seleccione un Tipo', 'a_alert text-center');
		document.getElementById('sel-tipo').classList.add('alert-input');
	}else if(UisSubtipo.selectedOptions[0].value === '0'){
		showAlert('Por favor seleccione un Subtipo', 'a_alert text-center');
	}else if(UisName.selectedOptions[0].value === '0'){
		showAlert('Por favor seleccione un Insumo', 'a_alert text-center');
	}else if(UisPresentacion.selectedOptions[0].value === '0'){
		showAlert('Por favor seleccione una Presentación', 'a_alert text-center');
	}else if(UiCantidad.value === ''){
		showAlert('Por favor indique una cantidad', 'a_alert text-center');
		UiCantidad.classList.add('alert-input');
  }else if(UiCantidad.value == 0){
		showAlert('Por favor Ingrese una cantidad mayor a 0','a_alert text-center');
    UiCantidad.classList.add('alert-input');
  }else if(!proveedor && inventario && (parseInt(UiCantidad.value) > parseInt(inventario.value))){
    showAlert('Cantidad ingresada supera al inventario disponible','a_alert text-center');
  }else if(inventario_ca && parseInt(UiCantidad.value) > parseInt(inventario_ca.value)){
    showAlert('Cantidad ingresada supera al inventario disponible','a_alert text-center');
  }else{
    UisTipo.disabled = true;
    if(UiBtnSubmit){
      UiBtnSubmit.disabled = false;
    }
		UiCantidad.disabled = true;
		UiCantidad.classList.remove('alert-input');
    if(nro_documento){
      nro_documento.readOnly = true;
      nro_documento.classList.remove('alert-input');
    }
    if(inventario){
      console.log('active');
      inventario.value = '';

    }
    if(inventario_ca){
      inventario_ca.value = '';
    }

    UisSubtipo.options[0].selected;
    const tr = document.createElement('tr');
    increment = increment +1;
    tr.className='myTr';
    tr.innerHTML = `
      <td>${UisTipo.selectedOptions[0].text}<input type="text" name="tipo[]" id="ins-type-${increment}" hidden value="${UisTipo.value}"></td>
      <td>${UisSubtipo.selectedOptions[0].text}<input type="text" name="subtipo[]" id="ins-subtype-${increment}" hidden value="${UisSubtipo.value}"></td>
      <td>${UisName.selectedOptions[0].text}<input type="text" hidden name="name[]" id="ins-desc-${increment}" value="${UisName.value}"></td>
      <td>${UisPresentacion.selectedOptions[0].text}<input type="text" hidden name="catalogo[]" id="ins-pres-${increment}" value="${UisPresentacion.value}"></td>
      <td>${UiCantidad.value}<input type="text" hidden name="cantidad[]" class="ins-cant" id="ins-cant-${increment}" value="${UiCantidad.value}"></td>
      <td>${UiPeso.value} KG<input type="text" hidden name="peso[]" class="ins-peso" id="ins-peso-${increment}" value="${UiPeso.value}"></td>
      <td>
        <div class="row clearfix text-center">
          <a href="#" class="deleteBtn"><i class="fa fa-lg fa-trash"></i></a>
        </div>
      </td>
    `;
    UiTable.appendChild(tr);
    UisSubtipo.value = 0;
    UisSubtipo.dispatchEvent(new Event('change'));
  }
}

function deleteEventHandler(e){
	// var checkingtable = document.getElementById('myTable');
  if(e.target.parentElement.classList.contains('deleteBtn')){
    e.target.parentElement.parentElement.parentElement.parentElement.remove();
    if(UiTable.childElementCount === 0){
      UisTipo.disabled = false;
      if(UiBtnSubmit){
        UiBtnSubmit.disabled = true;
        if(nro_documento){
          nro_documento.readOnly = false;
        }
      }
      cant_total.innerText = '0';
      peso_total.innerText = '0';
    }
	}
  e.preventDefault();
}

function checkNota(e){
  if(e.target.value.length < 8){
    const val = e.target.value.replace(/[^0-9]/g, '');
    e.target.value = val;
  }else{
    showAlert('La nota de recepcion no puede tener mas de 7 caracteres', 'a_alert');
  }
  if(e.target.value.match(/^[0-9]{4,7}$/g) !== null){
    e.target.value = e.target.value.match(/^[0-9]{4,7}$/g);
    console.log(e.target.value);
  }
  e.preventDefault();
}

function remove_highlight(){
  nro_documento.classList.remove('alert_input');
}

if(UiTable){
  if(UiNota){
    UiNota.addEventListener('keyup', checkNota)
  }
  Uibtn.addEventListener('click', addEventHandler);
  UiTable.addEventListener('click', deleteEventHandler);
}

if(window.location.search.split('=')[1] === 'wrong-password'){
  showAlert('Contrasena Incorrecta vuelva a intentar', 'a_alert text-center');
}

const insTable = document.querySelector('#insTable');
async function get(url){
  const rest = await fetch(url);
  const response = await rest.json();
  return response;
}

document.querySelector('#ins').addEventListener('click', (e) => {
  const idNote = e.target.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.textContent;

  get(`http://localhost/eco_insumo/index.php/consultas_ajax/getIns/${idNote}`)
    .then((data) => {
      // console.log(data[0].cantidad);
      let output = '';
      // const tr = document.createElement('tr');
      data.forEach((res) => {
        console.log(res);
        output += `
        <tr>
          <td>${res.tipo_insumo}</td>
          <td>${res.subtipo_insumo}</td>
          <td>${res.insumo}</td>
          <td>${res.cantidad}</td>
          <td>${res.peso_cantidad}</td>
        </tr>`
      });
      document.querySelector('#insTable').innerHTML = output;
      // console.log(document.querySelector('#insTable'));
    })
    .catch((err) => console.log(err.message));

});