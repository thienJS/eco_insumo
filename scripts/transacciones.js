const listEntregas = document.querySelector('#entregas');
const listTransferencias = document.querySelector('#transferencias');

document.addEventListener('DOMContentLoaded', () => {
  listEntregas.style.display = 'none';
  listTransferencias.style.display = 'none';
});

document.querySelector('#btn-entrega').addEventListener('click', () => {
  console.log('hi')
  toggleList('entregas');
});

document.querySelector('#btn-tranferencia').addEventListener('click', () => {
  console.log('hi2')
  toggleList('transferencias');
});

function toggleList(tgt){
  if(tgt === 'entregas'){
    listEntregas.style.display = 'block';
    listTransferencias.style.display = 'none';
  }else{
    listTransferencias.style.display = 'block';
    listEntregas.style.display = 'none';
  }
}