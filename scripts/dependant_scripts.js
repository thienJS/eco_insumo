
$(document).ready(function(){
    //################################## INFO DE INSUMO ############################################
    //Cambio en Tipo
    $('#s1').change(function(){
        var tipo = $(this).val();

        // AJAX request
         $.ajax({
            url:base_url+'index.php/consultas_ajax/get_subtipos_insumo',
            method: 'post',
            data: {this_tipo: tipo},
            dataType: 'json',
            success: function(response){

                // Remove options
                $('#s4').find('option').not(':first').remove();
                $('#s3').find('option').not(':first').remove();
                $('#s2').find('option').not(':first').remove();
                $('#inventario').val('');
				$('#cant').val('');
				$('#peso').val('');
                // Add options
                $.each(response,function(index,data){
                    $('#s2').append('<option value="'+data['id']+'">'+data['name']+'</option>');
                });
            }
        });
    });

    // Cambio en Subtipo
    $('#s2').change(function(){
        var subtipo = $(this).val();

        // AJAX request
         $.ajax({
            url:base_url+'index.php/consultas_ajax/get_insumos',
            method: 'post',
            data: {this_subtipo: subtipo},
            dataType: 'json',
            success: function(response){

                // Remove options
                $('#s4').find('option').not(':first').remove();
                $('#s3').find('option').not(':first').remove();
                $('#inventario').val('');
				$('#cant').val('');
				$('#peso').val('');
                // Add options
                $.each(response,function(index,data){
                    $('#s3').append('<option value="'+data['id']+'">'+data['name']+'</option>');
                });
            }
        });
    });

    // Cambio en Insumo
    $('#s3').change(function(){
        var insumo = $(this).val();
        // AJAX request
         $.ajax({
            url:base_url+'index.php/consultas_ajax/get_catalogo',
            method: 'post',
            data: {this_insumo: insumo},
            dataType: 'json',
            success: function(response){

                // Remove options
                $('#s4').find('option').not(':first').remove();
                $('#inventario').val('');
				$('#cant').val('');
				$('#peso').val('');
                // Add options
                $.each(response,function(index,data){
                    $('#s4').append('<option data-peso = "'+data['peso_unidad']+'" value="'+data['id']+'">'+data['presentacion']+' ('+data['peso_unidad']+' KG)</option>');
                });
            }
        });
    });
    const inv = document.querySelector('#inventario');

    // Cambio en Presentacion
    if(inv){
        console.log('exist');
        $('#s4').change(function(){
            var presentacion = $(this).val();
            console.log('Presentacion = '+presentacion);
            if($('#s4 option:selected').index() == 0){
                $('#inventario').val('');
                $('#cant').val('');
                $('#peso').val('');
                $('#cant').attr('disabled',true);
            }else{
                $('#cant').attr('disabled',false);
            }
            $.ajax({
                url:base_url+'index.php/consultas_ajax/get_inventario',
                method: 'post',
                data: {this_presentacion: presentacion},
                dataType: 'json',
                success: function(response){
                    //Fill Input
                    $('#inventario').val(response.inventario);
                }
            });
        });
    }else{
        console.log('ca');
        $('#s4').change(function(){
            var presentacion = $(this).val();
            console.log('Presentacion = '+presentacion);
            if($('#s4 option:selected').index() == 0){
                $('#inventario_ca').val('');
                $('#cant').val('');
                $('#peso').val('');
                $('#cant').attr('disabled',true);
            }else{
                $('#cant').attr('disabled',false);
            }
            $.ajax({
                url:base_url+'index.php/consultas_ajax/get_inventario_ca',
                method: 'post',
                data: {this_presentacion: presentacion},
                dataType: 'json',
                success: function(response){
                    console.log(`this: ${response}`);
                    //Fill Input
                    $('#inventario_ca').val(response.inventario);
                }
            });
        });
    }

    //######################################## FIN DE INFO DE INSUMO ####################################

    //#################################### INFO DE CENTROS DE ACOPIO ####################################
        //Cambio en Estado
        $('#e1').change(function(){
            var estado = $(this).val();
            console.log('estado = '+estado);
            // AJAX request
             $.ajax({
                url:base_url+'index.php/consultas_ajax/get_ca',
                method: 'post',
                data: {this_estado: estado},
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    // Remove options
					$('#e2').find('option').not(':first').remove();
					$('#municipio').val('');
					$('#parroquia').val('');
					$('#direccion').val('');
                    // Add options
                    $.each(response,function(index,data){
                        $('#e2').append('<option value="'+data['id']+'">'+data['name']+'</option>');
                    });
                }
            });
        });

        if('#ee1'){
            $('#ee1').change(function(){
                var estado = $(this).val();
                console.log('estado = '+estado);
                // AJAX request
                 $.ajax({
                    url:base_url+'index.php/consultas_ajax/get_ca',
                    method: 'post',
                    data: {this_estado: estado},
                    dataType: 'json',
                    success: function(response){
                        console.log(response);
                        // Remove options
                        $('#ee2').find('option').not(':first').remove();
                        // Add options
                        $.each(response,function(index,data){
                            $('#ee2').append('<option value="'+data['id']+'">'+data['name']+'</option>');
                        });
                    }
                });
            });
        }
        //Cambio en Centro de Acopio para Municipio
        $('#e2').change(function(){
            var ca = $(this).val();
            console.log('ca = '+ca);
            // AJAX request
             $.ajax({
                url:base_url+'index.php/consultas_ajax/get_municipios',
                method: 'post',
                data: {this_ca: ca},
                dataType: 'json',
                success: function(response){
					console.log(response);
					$('#municipio').val('');
                    //Anexa Municipio al input
                    $('#municipio').val(response.municipio);

                }
            });
        });

        //Cambio en Centro de Acopio para Parroquia
        $('#e2').change(function(){
            var ca = $(this).val();
            console.log('ca = '+ca);
            // AJAX request
             $.ajax({
                url:base_url+'index.php/consultas_ajax/get_parroquias',
                method: 'post',
                data: {this_ca: ca},
                dataType: 'json',
                success: function(response){
					console.log(response);
					$('#parroquia').val('');
                    //Anexa parroquia al input
                    $('#parroquia').val(response.parroquia);
                }
            });
        });

        //Cambio en Centro de Acopio para direccion
        $('#e2').change(function(){
            var ca = $(this).val();
            console.log('ca = '+ca);
            // AJAX request
             $.ajax({
                url:base_url+'index.php/consultas_ajax/get_direccion',
                method: 'post',
                data: {this_ca: ca},
                dataType: 'json',
                success: function(response){
					console.log(response);
					$('#direccion').val('');
                    //Anexa Direccion al textArea
                    $('#direccion').val(response.direccion);

                }
            });
        });

    //############################### FUERZAS PRODUCTIVAS #################################################################

         // cambio en estado para obtener fuerza productiva
         $('#e').change(function(){
            var estado = $(this).val();
            console.log('estado = '+estado);
            // AJAX request
            $.ajax({
                url:base_url+'index.php/consultas_ajax/get_fp',
                method: 'post',
                data: {this_estado: estado},
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    // Remove options
					$('#fp').find('option').not(':first').remove();

                    // Add options
                    $.each(response,function(index,data){
                        $('#fp').append('<option value="'+data['id_fp']+'">'+data['representante']+'</option>');
                    });
                }
            });
        });

        // seleccionar fuerza productiva para obtener datos del representante
        $('#fp').change(function(){
            var fp = $(this).val();
            console.log('fp = '+fp);
            // AJAX request
            $.ajax({
                url:base_url+'index.php/consultas_ajax/get_fp_representante',
                method: 'post',
                data: {this_fp: fp},
                dataType: 'json',
                success: function(response){

                    console.log(response);

                    // Add options
                    $.each(response,function(index,data){
                        const names = document.querySelector('#names');
                        const ci = document.querySelector('#ci');
                        const tlf = document.querySelector('#tlf');
                        names.value = data['representante'];
                        ci.value = data['rif_cedula'];
                        tlf.value = data['telefono'];
                    });
                }
            });
        });

        $('#e2').change(function(){
            var ca = $(this).val();
            console.log('ca = '+ca);
            // AJAX request
             $.ajax({
                url:base_url+'index.php/consultas_ajax/get_representante',
                method: 'post',
                data: {this_ca: ca},
                dataType: 'json',
                success: function(response){
					console.log(response);
					$('#names').val('');
					$('#ci').val('');
                    $('#tlf').val('');

                    // agrega datos del reperesentate ///////////////////

                    $('#names').val(response[0]['nombre'] + ' ' + response[0]['apellido']);
                    $('#ci').val(response[0]['cedula']);
					$('#tlf').val(response[0]['telefono']);
                }
            });
        });

    //############################### FIN DE FUERZAS PRODUCTIVAS ##########################################################
	//################################# FIN DE INFO DE CENTROS DE ACOPIO ##################################################
	//################################### DESPUES DE INSERTAR UN INSUMO EN LA TABLA ########################################
		 $('#myElement').click(function(){
        //$('#s2').val(0).trigger('change');
            var total_cant = parseInt(0);
            var total_peso = parseInt(0);
            if($('#myTable tr')){
                $('#myTable tr').each(function(){
                    var current_cant = $(this).find('td .ins-cant').val();
                    var current_peso = $(this).find('td .ins-peso').val();
                    total_cant = parseInt(total_cant) + parseInt(current_cant);
                    total_peso = parseInt(total_peso) + parseInt(current_peso);
                    $('#cant-total').text(total_cant);
                    $('#peso-total').text(total_peso);
                })
            }
		  });
	//################################## FIN DE INSERTAR INSUMO EN LA TABLA ###############################################
});
