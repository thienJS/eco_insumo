$(document).ready(function() {
    $(".number_valid").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1) {
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

// function limit(element)
// {
//     var max_chars = 8;

//     if(element.value.length > max_chars) {
//         element.value = element.value.substr(0, max_chars);
//     }
// }
