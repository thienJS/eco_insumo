  if(document.querySelector('#agroquimicos')){
    const agro = document.querySelector('#agroquimicos').textContent;
    const fert = document.querySelector('#fertilizantes').textContent;
    const sem = document.querySelector('#semillas').textContent;

    console.log(agro, fert, sem);
    $(function () {
      "use strict";
      //DONUT CHART
      var donut = new Morris.Donut({
        element: 'sales-chart',
        resize: true,
        colors: ["#3c8dbc", "#f56954", "#00a65a"],
        data: [
          {label: "Agroquimicos", value: agro},
          {label: "Fertilizantes", value: fert},
          {label: "semillas", value: sem}
        ],
        hideHover: 'auto'
      });
    });
  }
