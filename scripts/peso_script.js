 $(document).ready(function(){
    $('#cant , #s4').on('change, keyup',function(){
        var data_peso = parseFloat($('#s4').find('option:selected').attr('data-peso')).toFixed(2);
        var cantidad = $('#cant').val();

        var result = data_peso*cantidad;

        $('#peso').val(result);
    });
}); 
