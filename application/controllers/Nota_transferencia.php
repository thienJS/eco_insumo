<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nota_transferencia extends CI_Controller {

	public function index()
	{
		if(!$this->session->userdata('session_status')){
			redirect('logins/login');
		}elseif(!$this->session->jefe_id){
			$this->session->set_flashdata('jefe_id','Usted es Jefe de Centro de Acopio');
			redirect ('home');
		}
		$data['title'] = "NOTA DE TRASFERENCIA";

		$data['tipos'] = $this->select_insumo_model->get_tipos_insumo();
		$data['estados'] =$this->select_estado_model->get_estados();
		$data['sessionJefe'] = $this->nota_transferencia_model->get_current_ca($this->session->jefe_id);

		if($_SERVER['REQUEST_METHOD'] === 'POST'){
			$this->nota_transferencia_model->db->trans_start();
			$lastIdVehiculo = $this->nota_recepcion_model->create_vehiculo();
			$lastIdTransportista = $this->nota_recepcion_model->create_transportista($lastIdVehiculo);
			$lastIdNota = $this->nota_transferencia_model->create_nota_transferencia($lastIdTransportista);
			$count = count($this->input->post('catalogo[]'));
			for($i=0;$i<$count;$i++){
				$this->nota_transferencia_model->create_nota_t_x_catalogo($lastIdNota,$i);
			}
			$this->nota_transferencia_model->db->trans_complete();

			redirect('index.php/nota_transferencia');
		}else{
			$this->load->view('templates/header');
			$this->load->view('templates/navigator');
			$this->load->view('nota_transferencia/index',$data);
			$this->load->view('templates/footer');
		}
	}
}
