<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entregas extends CI_Controller {

	public function __construct(){
    parent::__construct();
    $this->load->helper(array('url', 'form'));
	}

	public function index(){
		$data['title'] = 'Entregas Realizadas';
		$data['entregas'] = $this->entrega_model->entregas(5);

		$this->load->view('templates/header');
		$this->load->view('templates/navigator');
		$this->load->view('entregas/index',$data);
		$this->load->view('templates/footer');
	}
}