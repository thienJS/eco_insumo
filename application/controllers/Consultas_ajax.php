<?php
    class Consultas_ajax extends CI_Controller{

    //############################### SELECTS DEPENDIENTES #################################
        //Carga el modelo para subtipo dependiente del tipo de insumo
        public function get_subtipos_insumo(){
            $postData = $this->input->post();
            $data = $this->select_insumo_model->get_subtipos_insumo($postData);
            echo json_encode($data);
        }

        //Carga el modelo para insumo dependiente del subtipo de insumo
        public function get_insumos(){
            $postData = $this->input->post();
            $data = $this->select_insumo_model->get_insumos($postData);
            echo json_encode($data);
        }

        //Carga el modelo para presentacion dependiente del insumo
        public function get_catalogo(){
            $postData = $this->input->post();
            $data = $this->select_insumo_model->get_catalogo($postData);
            echo json_encode($data);
        }

        public function get_representante(){
            $postData = $this->input->post();
            $data = $this->nota_transferencia_model->get_representante($postData);
            echo json_encode($data);
        }

        //Carga el modelo para inventario dependiente de la presentacion
        public function get_inventario(){
            $postData = $this->input->post();
            $data = $this->select_insumo_model->get_inventario($postData);
            echo json_encode($data);
        }

        public function get_inventario_ca(){
            $postData = $this->input->post();
            $data = $this->select_insumo_model->get_inventario_ca($postData);
            echo json_encode($data);
        }

        //Carga el modelo para municipios dependientes del estado
        public function get_municipios(){
            $postData = $this->input->post();
            $data = $this->select_estado_model->get_municipios($postData);
            echo json_encode($data);
        }

        //Carga el modelo para municipios dependientes del estado
        public function get_parroquias(){
            $postData = $this->input->post();
            $data = $this->select_estado_model->get_parroquias($postData);
            echo json_encode($data);
        }

        //Carga el modelo para municipios dependientes del estado
        public function get_ca(){
            $postData = $this->input->post();
            $data = $this->select_estado_model->get_ca($postData);
            echo json_encode($data);
        }

        //Llena el textarea de direccion despues del select CA
        public function get_direccion(){
            $postData = $this->input->post();
            $data = $this->select_estado_model->get_direccion($postData);
            echo json_encode($data);
        }

         // carga el model para fuerzas productivas dependientes del estado
         public function get_fp(){
            $postData = $this->input->post();
            $data = $this->nota_entrega_model->get_fp($postData);
            echo json_encode($data);
        }

        public function get_fp_representante(){
            $postData = $this->input->post();
            $data = $this->nota_entrega_model->get_fp_representante($postData);
            echo json_encode($data);
        }

        public function getIns($id){
            $data = $this->entrega_model->getIns($id);
            echo json_encode($data);
        }
	//############################### FIN DE SELECTS DEPENDIENTES #################################

	################################# FACTURAS MODAL ##############################################
	    public function insumos_factura(){
	    	$postData = $this->input->post();
	    	$data = $this->lista_facturas_model->get_insumos_factura($postData);
	    	echo json_encode($data);
	    }
	################################# FIN FACTURAS MODAL ##########################################


    ################################ ASIGNACIONES MODAL ###########################################
        public function insumos_asignacion(){
            $postData = $this->input->post();
            $data = $this->lista_asignaciones_model->get_insumos_asignacion($postData);
            echo json_encode($data);
        }
    ############################### FIN DE ASIGNACIONES MODAL #####################################
    }
?>
