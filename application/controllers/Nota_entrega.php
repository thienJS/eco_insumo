<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nota_entrega extends CI_Controller {
	public function index()
	{
		if(!$this->session->userdata('session_status')){
			redirect('logins/login');
		}elseif(!$this->session->jefe_id){
			$this->session->set_flashdata('jefe_id','Usted es Jefe de Centro de Acopio');
			redirect ('home');
		}
		$data['title'] = "NOTA DE ENTREGA";
		$data['ca'] = $this->nota_recepcion_model->get_ca($this->session->ca_id);
		$data['tipos'] = $this->select_insumo_model->get_tipos_insumo();
		$data['estados'] = $this->nota_entrega_model->get_fuerzasp_estados();
		$data['sessionJefe'] = $this->nota_transferencia_model->get_current_ca($this->session->jefe_id);
		if($_SERVER['REQUEST_METHOD'] === 'POST'){
			if($this->input->post('nota') === ''){
				$this->session->set_flashdata('user_loggedin','Por favor ingrese la nota de entrega o correlativo');
				redirect ('index.php/nota_entrega/index');
			}else{
				$this->nota_entrega_model->db->trans_start();
				$lastIdVehiculo = $this->nota_recepcion_model->create_vehiculo();
				$lastIdTransportista = $this->nota_recepcion_model->create_transportista($lastIdVehiculo);
				$lastIdNota = $this->nota_entrega_model->create_nota_entrega($lastIdTransportista);
				$count = count($this->input->post('catalogo[]'));
				$catalogo = $this->input->post('catalogo');
				$ca = $this->session->ca_id;
				for($i=0;$i<$count;$i++){
					$this->nota_entrega_model->create_nota_e_x_catalogo($lastIdNota,$i);
					$c_p = $this->inventario_ca_model->select_cant_peso($ca, $catalogo, $i);
					$this->inventario_ca_model->update_inventario_ca_minus($ca, $i, intval($c_p[0]['cantidad']), intval($c_p[0]['peso']));
				}
				$this->nota_entrega_model->db->trans_complete();

				redirect('home');
			}
		}else{
			$this->load->view('templates/header');
			$this->load->view('templates/navigator');
			$this->load->view('nota_entrega/index',$data);
			$this->load->view('templates/footer');

		}
	}
}
