<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Despacho extends CI_Controller {

	public function __construct(){
    parent::__construct();
    $this->load->helper(array('url', 'form'));
	}

	public function index(){
		$data['title'] = 'Despacho';
		$data['entregas'] = $this->entrega_model->entregas(6);

		$this->load->view('templates/header');
		$this->load->view('templates/navigator');
		$this->load->view('despacho/index', $data);
		$this->load->view('templates/footer');
	}

	public function entregas(){
		$data['title'] = 'Despacho de Entregas';

		$this->load->view('templates/header');
		$this->load->view('templates/navigator');
		$this->load->view('despacho/entregas',$data);
		$this->load->view('templates/footer');
	}

	public function transferencias(){
		$data['title'] = 'Despacho de Transferencias';

		$this->load->view('templates/header');
		$this->load->view('templates/navigator');
		$this->load->view('despacho/transferencias',$data);
		$this->load->view('templates/footer');
	}

}