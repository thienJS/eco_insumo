<?php
    class Admins extends CI_Controller{ //TEST BRANCH
    //################################# CARGA DE FACTURAS ##########################################

        public function carga_facturas(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['title'] = 'CARGA DE FACTURA';
			$data['tipos'] = $this->select_insumo_model->get_tipos_insumo();
			$data['proveedores'] = $this->select_insumo_model->get_proveedores();

            $this->form_validation->set_rules('nro_factura','Numero de Factura','required');
            $this->form_validation->set_rules('catalogo[]','Presentacion','required');
            $this->form_validation->set_rules('cantidad[]','Cantidad','required');

            if($this->form_validation->run()===FALSE){
                $this->load->view('templates/header');
                $this->load->view('templates/navigator');
				$this->load->view('admins/facturas/carga_facturas',$data);
                $this->load->view('templates/footer');
            }else{
                $count = count($this->input->post('catalogo[]'));

                $this->admin_model->db->trans_start();

                $last_id = $this->admin_model->crear_factura();

                if($last_id > 0){
                    for($i=0;$i<$count;$i++){
                        $this->admin_model->insumos_factura($last_id,$i);
                    }
                    $this->admin_model->db->trans_complete();
                    redirect('admins/inventario_insumos');
                }else{
                    $this->session->set_flashdata('receipt_exists','Esta Factura Ya existe');
                    redirect('admins/carga_facturas');
                }
            }
        }
        //Carga la Vista de Lista de Facturas
        public function lista_facturas(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['title'] = 'LISTA DE FACTURAS';
			$data['lista_facturas'] = $this->lista_facturas_model->get_lista_facturas();
			//$data['insumos_factura'] = $this->lista_facturas_model->get_insumos_factura();

            $this->load->view('templates/header');
            $this->load->view('templates/navigator');
			$this->load->view('admins/facturas/lista_facturas',$data);
			$this->load->view('modals/factura_modal',$data);
            $this->load->view('templates/footer');


		}

    //############################### FIN DE CARGA DE FACTURAS ##############################

    ################################# ASIGNACION A CENTROS DE ACOPIO #################################
        //Crea una Nota de Asignacion para un Centro de Acopio
        public function asignacion_insumos(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['title'] = "ASIGNACION DE INSUMOS A CENTRO DE ACOPIO";
            $data['tipos'] = $this->select_insumo_model->get_tipos_insumo();
            $data['estados'] =$this->select_estado_model->get_estados();

            $this->form_validation->set_rules('nro_asignacion','Numero de Asignacion','required');
            $this->form_validation->set_rules('catalogo[]','Presentacion','required');
            $this->form_validation->set_rules('cantidad[]','Cantidad','required');

            if($this->form_validation->run()===FALSE){
                $this->load->view('templates/header');
                $this->load->view('templates/navigator');
                $this->load->view('admins/asignaciones/asignacion_insumos',$data);
                $this->load->view('templates/footer');
            }else{
                $count = count($this->input->post('catalogo[]'));
                $this->admin_model->db->trans_start();

                $last_id =$this->admin_model->crear_asignacion();

                for($i=0;$i<$count;$i++){
                    $this->admin_model->insumos_asignacion($last_id,$i);
                }

                $this->admin_model->db->trans_complete();
                redirect('admins/inventario_insumos');
            }
        }
        //Carga la Vista de Lista de Asignaciones
        public function lista_asignaciones(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['title'] = 'LISTA ASIGNACIONES';
            $data['lista_asignaciones'] = $this->lista_asignaciones_model->get_lista_asignaciones();

            $this->load->view('templates/header');
            $this->load->view('templates/navigator');
            $this->load->view('admins/asignaciones/lista_asignaciones',$data);
            $this->load->view('modals/asignacion_modal',$data);
            $this->load->view('templates/footer');
        }

    ################################ FIN DE ASIGNACION A CENTROS DE ACOPIO ############################

    //##############################  INVENTARIO DE INSUMOS #################################
        //Carga la vista de inventario de insumos
        public function inventario_insumos(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['title'] = 'Inventario de Insumos';
            $data['inventarios'] = $this->inventario_model->get_inventario();

            $this->load->view('templates/header');
            $this->load->view('templates/navigator');
            $this->load->view('admins/listados/inventario_insumos',$data);
            $this->load->view('templates/footer');
        }
    //############################### FIN DE INVENTARIO DE INSUMOS ##############################

    //##############################  CATALOGO DE INSUMOS #################################
        //Carga la vista de inventario de insumos
        public function catalogo_insumos(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['title'] = 'Catalogo de Insumos';
            $data['catalogos'] = $this->catalogo_model->get_catalogo();
            $data['tipos'] = $this->select_insumo_model->get_tipos_insumo();
            $data['tipos_presentacion'] = $this->select_insumo_model->get_tipo_presentacion();

            $this->load->view('templates/header');
            $this->load->view('templates/navigator');
            $this->load->view('admins/listados/catalogo_insumos',$data);
            $this->load->view('modals/catalogo_modal',$data);
            $this->load->view('templates/footer');

        }
    //############################### FIN DE CATALOGO DE INSUMOS ##############################

    //##############################  LISTA DE INSUMOS #################################
        //Carga la vista de inventario de insumos
        public function lista_insumos(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['title'] = 'Lista de Insumos';
            $data['linsumos'] = $this->lista_insumos_model->get_lista_insumos();
            $data['tipos'] = $this->select_insumo_model->get_tipos_insumo();

            $this->load->view('templates/header');
            $this->load->view('templates/navigator');
            $this->load->view('admins/listados/lista_insumos',$data);
            $this->load->view('modals/insumo_modal',$data);
            $this->load->view('templates/footer');
        }
    //############################### FIN DE LISTA DE INSUMOS ##############################


    //############################### NUEVOS REGISTROS #######################################
        //CREA UN NUEVO INSUMO EN CATALOGO
        public function nuevo_catalogo(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['catalogos'] = $this->catalogo_model->get_catalogo();
            $data['tipos'] = $this->select_insumo_model->get_tipos_insumo();
            $data['tipos_presentacion'] = $this->select_insumo_model->get_tipo_presentacion();

            $this->form_validation->set_rules('id_tipo_insumo','Tipo de Insumo','required');
            $this->form_validation->set_rules('id_subtipo_insumo','Subtipo de Insumo','required');
            $this->form_validation->set_rules('id_insumo','Insumo','required');
            $this->form_validation->set_rules('presentacion','Presentacion','required');
            $this->form_validation->set_rules('peso_unidad','Peso Unitario','required');

            if($this->form_validation->run()===FALSE){
                $this->load->view('templates/header');
                $this->load->view('templates/navigator');
                $this->load->view('admins/listados/catalogo_insumos',$data);
                $this->load->view('templates/footer');
            }else{
                $lastId = $this->catalogo_model->create_catalogo();
                $centro_acopios = $this->nota_recepcion_model->get_ids_ca();
                foreach($centro_acopios as $ca){
                    $this->inventario_ca_model->insert_inventario($ca['id'], $lastId);
                }
                redirect('index.php/admins/catalogo_insumos');
            }
        }
        //CREA UN NUEVO INSUMO
        public function nuevo_insumo(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['linsumos'] = $this->lista_insumos_model->get_lista_insumos();
            $data['tipos'] = $this->select_insumo_model->get_tipos_insumo();

            $this->form_validation->set_rules('id_tipo_insumo','Tipo de Insumo','required');
            $this->form_validation->set_rules('id_subtipo_insumo','Subtipo de Insumo','required');
            $this->form_validation->set_rules('insumo','Insumo','required');

            if($this->form_validation->run()===FALSE){
                $this->load->view('templates/header');
                $this->load->view('templates/navigator');
                $this->load->view('admins/listados/lista_insumos',$data);
                $this->load->view('templates/footer');
            }else{
                $this->lista_insumos_model->create_insumo();
                redirect('admins/lista_insumos');
            }
        }
    ###########################################################################################

    ################################## REPORTE SEMAFORO #######################################
        public function reporte_semaforo(){
            if(!$this->session->userdata('session_status')){
                redirect('logins/login');
            }
            $data['title'] = 'REPORTE SEMAFORO';
            $data['entregas'] = $this->admin_model->get_ultima_entrega();
            $this->load->view('templates/header');
            $this->load->view('templates/navigator');
            $this->load->view('admins/listados/semaforo',$data);
            $this->load->view('templates/footer');
        }
    ################################### FIN REPORTE SEMAFORO ##################################
    }
?>
