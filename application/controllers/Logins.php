<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  class Logins extends CI_Controller {

  //   public function createUserSession($user){
  //     $_SESSION['userId'] = $user[0]['id_user'];
  //     $_SESSION['userName'] = $user[0]['username'];
  //     // $_SESSION['userEmail'] = $user['email'];
  //     // $_SESSION['userIdPerson'] = $user['id_person'];
  //     redirect('index.php/pages/view');
  //   }

  //   public function index($page = 'login'){
  //     if(!file_exists(APPPATH. 'views/login/'.$page.'.php')){
  //       show_404();
  //     }
  //     $data['title'] = ucfirst($page);

  //     if($_SERVER['REQUEST_METHOD'] === 'POST'){
  //       if($this->login_model->check_user() === 1){
  //         $this->createUserSession($this->login_model->login_user());
  //       }else{
  //         redirect('index.php/logins/?pw=wrong-password');
  //       }
  //     }else{
  //       $this->load->view('templates/header');
  //       $this->load->view('login/'.$page, $data);
  //       $this->load->view('templates/footer');
  //     }
  //   }
  // }

    public function login(){
      if($this->session->userdata('session_status')){
        redirect('home');
      }

      $data['title'] = 'Iniciar Sesion';

      $this->form_validation->set_rules('username','Nombre de Usuario','required');
      $this->form_validation->set_rules('password','Contrasenia','required');
      // $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback_recaptcha');

      if($this->form_validation->run() === FALSE){
        $this->load->view('templates/header');
        $this->load->view('templates/navigator');
        $this->load->view('login/login',$data);
        $this->load->view('templates/footer');
      }else{
        //nombre de usuario
        $username = $this->input->post('username');
        //contrasenia
        $password = $this->input->post('password');
        //ID de usuario
        $user = $this->login_model->login($username,$password);

        if($user){
          $user_data = array(
            'user_id' => $user[0]['userid'],
            'jefe_id' => $user[0]['jefeid'],
            'ca_id' => $user[0]['caid'],
            'session_status' => TRUE
          );
          $this->session->set_userdata($user_data);
          //MENSAJE DE LOGIN
          $this->session->set_flashdata('user_loggedin','Has inciado Sesion');
            redirect ('home');
        }else{
          $this->session->set_flashdata('login_failed','Usuario o Contrasenia Incorrecto(s)');
            redirect ('logins/login');
        }
      }
    }

    // public function recaptcha($str=''){
    //   $google_url="https://www.google.com/recaptcha/api/siteverify";
    //   $secret='6LdYf2QUAAAAAIXyuH9f4F6Lcs_JhVk5x4wrGs70';
    //   $ip=$_SERVER['REMOTE_ADDR'];
    //   $url=$google_url."?secret=".$secret."&response=".$str."&remoteip=".$ip;
    //   $curl = curl_init();
    //   curl_setopt($curl, CURLOPT_URL, $url);
    //   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //   curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    //   curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
    //   $res = curl_exec($curl);
    //   curl_close($curl);
    //   $res= json_decode($res, true);
    //   //reCaptcha success check
    //   if($res['success']){
    //     return TRUE;
    //   }else{
    //     $this->form_validation->set_message('recaptcha', 'Por favor rellene el ReCaptcha antes de continuar');
    //     return FALSE;
    //   }
    // }

    public function logout(){
      $this->session->unset_userdata('session_status');
      $this->session->unset_userdata('user_id');
      $this->session->unset_userdata('jefe_id');
      $this->session->unset_userdata('ca_id');

      $this->session->set_flashdata('logged_out','Has Cerrado Sesion');
      redirect ('logins/login');
    }
  }

?>