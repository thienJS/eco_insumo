<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));

	}

	public function index(){
		$data['title'] = 'Reportes';
		$data['agroquimicos'] = $this->reporte_model->agroquimicos();
		$data['fertilizantes'] = $this->reporte_model->fertilizantes();
		$data['semillas'] = $this->reporte_model->semillas();
		// $data['inventarios'] = $this->inventario_ca_model->get_inventario();

		$this->load->view('templates/header');
		$this->load->view('templates/navigator');
		$this->load->view('reportes/index',$data);
		$this->load->view('templates/footer');
	}
}