<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nota_recepcion extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		if(!$this->session->userdata('session_status')){
			redirect('logins/login');
		}
		$data['title'] = "NOTA DE RECEPCION";
		$data['tipos'] = $this->select_insumo_model->get_tipos_insumo();
		$data['ca'] = $this->nota_recepcion_model->get_ca($this->session->ca_id);
		$data['asignaciones'] = $this->nota_recepcion_model->get_asignaciones($this->session->ca_id);
		//print_r($this->session->ca_id);die;

		if($_SERVER['REQUEST_METHOD'] === 'POST'){
			$this->nota_recepcion_model->db->trans_start();
			$this->nota_recepcion_model->update_asignacion();
			$lastIdVehiculo = $this->nota_recepcion_model->create_vehiculo();
			$lastIdTransportista = $this->nota_recepcion_model->create_transportista($lastIdVehiculo);
			$lastIdNota = $this->nota_recepcion_model->create_nota_recepcion($lastIdTransportista);
			$count = count($this->input->post('catalogo[]'));
			$catalogo = $this->input->post('catalogo');
			$ca = $this->session->ca_id;
			for($i=0;$i<$count;$i++){
				$this->nota_recepcion_model->create_notaxcatalogo($lastIdNota,$i);
				$c_p = $this->inventario_ca_model->select_cant_peso($ca, $catalogo, $i);
				$this->inventario_ca_model->update_inventario_ca($ca, $i, intval($c_p[0]['cantidad']), intval($c_p[0]['peso']));
			}
			$this->nota_recepcion_model->db->trans_complete();
			redirect ('home');
		}else{
			$this->load->view('templates/header');
			$this->load->view('templates/navigator');
			$this->load->view('nota_recepcion/index',$data);
			$this->load->view('templates/footer');
		}
	}

	public function transferencias(){
		$data['transferencias'] = $this->nota_recepcion_model->get_transferencias_pendientes();
		$this->load->view('templates/header');
		$this->load->view('templates/navigator');
		$this->load->view('nota_recepcion/transferencias', $data);
		$this->load->view('templates/footer');
	}

	public function r_transferencias($id){
		if($_SERVER['REQUEST_METHOD'] === 'POST'){
			$this->inventario_ca_model->db->trans_start();
			$count = count($this->input->post('catalogo[]'));
			$catalogo = $this->input->post('catalogo');
			$ca = $this->session->ca_id;
			for($i=0;$i<$count;$i++){
				$c_p = $this->inventario_ca_model->select_cant_peso($ca, $catalogo, $i);
				$this->inventario_ca_model->update_inventario_ca($ca, $i, intval($c_p[0]['cantidad']), intval($c_p[0]['peso']));
			}
			$this->nota_transferencia_model->create_recepcion_transferencia($id);
			$this->nota_transferencia_model->update_nt_status($id);
			$this->inventario_ca_model->db->trans_complete();
			redirect('inventario_ca');
		}else{
			$data['tipos'] = $this->select_insumo_model->get_tipos_insumo();
			$data['nt_data'] = $this->nota_recepcion_model->get_transferencias_data($id);
			$this->load->view('templates/header');
			$this->load->view('templates/navigator');
			$this->load->view('nota_recepcion/r_transferencia', $data);
			$this->load->view('templates/footer');
		}
	}
}
