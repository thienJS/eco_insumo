<!-- Modal -->
<div id="asignacionModal" class="modal fade" role="dialog">
  	<div class="modal-lg modal-dialog">
    	<!-- Modal content-->
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 id="asignacion-label" class="modal-title text-center"></h4>
      		</div>
      		<div class="modal-body">
        		<table id="insumos_asignacion" class="table table-bordered">
					<thead>
						<tr>
							<th>ID</th>
							<th>TIPO</th>
							<th>SUBTIPO</th>
							<th>NOMBRE</th>
							<th>PRESENTACION</th>
							<th>CANTIDAD</th>
							<th>PESO</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cerrar</button>
      		</div>
    	</div>
  	</div>
</div>