<!-- Modal -->
<div id="catalogoModal" class="modal fade" role="dialog">
  <div class="modal-lg modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">AGREGAR INSUMO A CATALOGO</h4>
      </div>
      <div class="modal-body">
      <?=validation_errors();?>
            <?=form_open('admins/nuevo_catalogo');?>
            <div class="form-group">
                <div class="row" id="myCol">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label class="form-label">Tipo</label>
                            <select class="form-control select2" id="s1" name="id_tipo_insumo">
                                <option>--Seleccione Tipo de Insumo--</option>
                                <?php foreach($tipos as $tipo):?>
                                    <option value="<?=$tipo['id'];?>"><?=$tipo['name'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label class="form-label">Subtipo</label>
                            <select class="form-control select2" id="s2" name="id_subtipo_insumo">
                                <option>--Seleccione Subtipo de Insumo--</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label class="form-label">Insumo</label>
                            <select class="form-control select2" id="s3" name="id_insumo">
                                <option>--Seleccione Insumo--</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label class="form-label">Presentacion</label>
                            <input class="form-control" type="text" name="presentacion">
                        </div>
                        <div class="col-sm-4">
                            <label>Tipo Presentacion</label>
                            <select class="form-control" name="id_tipo_presentacion">
                                <?php foreach($tipos_presentacion as $tipo_presentacion):?>
                                    <option value="<?=$tipo_presentacion['id'];?>"><?=$tipo_presentacion['name'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label>Peso Unitario</label>
                            <input class="form-control" type="text" name="peso_unidad">
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Registrar</button>
          <?=form_close();?>
      </div>
    </div>

  </div>
</div>