<section class="content">

	<div class="row">
		<div class="col-sm-6">
			<button id="btn-entrega" class="btn btn-primary btn-block"><strong>Entregas</strong></button>
		</div>
		<div class="col-sm-6">
			<button id="btn-tranferencia" class="btn btn-primary btn-block"><strong>Transferencias</strong></button>
		</div>
	</div>
	<div id="entregas">
		<div class="box-body">
			<?php if(empty($entregas)) :?>
				<h2 class="text-center">No hay Entregas Pendientes</h2>
			<?php else : ?>
			<table class="dt table table-striped table-hover table-bordered" style="width:100%">
				<thead>
					<tr>
							<th>ID</th>
							<th>CORRELATIVO DE ENTREGA</th>
							<th>FECHA</th>
							<th>FUERZA PRODUCTIVA</th>
							<th></th>
					</tr>
				</thead>
					<?php foreach($entregas as $entrega): ?>
						<tr>
								<td><?=$entrega['id_nota_entrega']?></td>
								<td><?=$entrega['nota_entrega']?></td>
								<td><?=$entrega['fecha']?></td>
								<td><?=$entrega['fp']?></td>
								<td>
								<button type="button" id="ins" class="btn" data-toggle="modal" data-target="#modal-default">
										Ver
								</button>
								</td>
						</tr>
						<?php endforeach; ?>
				<tbody>
				</tbody>
			</table>
			<?php endif; ?>
		</div>
	</div>
	<div id="transferencias">
		<div class="box-body">
			<table class="dt table table-striped table-hover table-bordered" style="width:100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>CORRELATIVO DE TRANSFERENCIA</th>
						<th>FECHA</th>
						<th>CENTRO DE ACOPIO</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</section>