<section class="content">
  <div class="box">
    <form action="" method="POST">
      <div class="box-header nav-custom text-center">
        <h3 class="box-title"><?=$title?></h3>
      </div>
      <div class="box-body">
				<div class="row">
					<div class="form-group">
						<div class="col-sm-6">
								<label class="form-label" >Nota de Entrega</label>
								<input id="nota" name="nota" type="text" class="form-control number_valid" value="" maxlength="7" readonly>
						</div>
						<div class="col-sm-6">
								<label class="form-label">Fecha</label>
								<input type="text" class="form-control" value="<?=date('d-m-Y')?>" readonly>
						</div>
					</div>
				</div>
			</div>
    </form>
  </div>
</section>
