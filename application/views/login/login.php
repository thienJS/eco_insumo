<section class="content altura">

  <form action="" method="POST">
    <div class="login-box">
      <!-- /.login-logo -->
      <div class="login-box-body">
        <form action="" method="post">
          <div class="login-logo">
            <a href="#"><img src="<?php echo base_url(); ?>assets/img/corpo.png" style="height: 150px; margin-top:-20px" alt=""></a>
          </div>
          <h5 class="login-box-msg" style="color: black"><strong>Sistema de insumos Corporacion DELAGRO</strong></h5>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="username" placeholder="Nombre de Usuario">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Contrasenia">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <!-- /.col -->
            <div class="col-xs-12">
              <!-- <div id="captcha-div">
                <
                <div class="g-recaptcha" data-sitekey="6LdYf2QUAAAAAG5Y3UeUKMuCe7CJ7DXOyPaQXh9O"></div>
              </div> -->
              <button style="margin-bottom: 20px " type="submit" class="btn btn-primary btn-block ">Iniciar Sesion</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

        <!-- /.social-auth-links -->

        <a href="#">Olvido su contrasena?</a><br>

      </div>
      <!-- /.login-box-body -->
    </div>
  </form>
</section>