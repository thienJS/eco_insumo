<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <p id="agro"></p>
      <!-- DONUT CHART -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3>Insumos Totales en la corporacion</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body chart-responsive">
          <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col (LEFT) -->
    <!-- /.col (RIGHT) -->
  </div>
  <!-- /.row -->
  <p class="text" id="agroquimicos" hidden><?= $agroquimicos[0]['insumos']; ?></p>
  <p class="text" id="fertilizantes" hidden><?= $fertilizantes[0]['insumos']; ?></p>
  <p class="text" id="semillas" hidden><?= $semillas[0]['insumos']; ?></p>
</section>
