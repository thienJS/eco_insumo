<?php if($this->session->userdata('session_status')): ?>
  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>E</b>P</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Eco</b>Pro</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top nav-custom">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url();?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Alexander Pierce</span>
            </a>
            <ul class="dropdown-menu user-border">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                <p>
                  Alexander Pierce
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?= base_url()?>logins/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU DE NAVEGACION</li>
        <li class="active">
          <a href="<?=base_url();?>/home">
            <i class="fa fa-home"></i> <span>Inicio</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-desktop"></i> <span>Administrador</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Cargar Factura
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>admins/carga_facturas"><i class="fa fa-circle-o"></i> Nueva Factura</a></li>
                <li><a href="<?=base_url();?>admins/lista_facturas"><i class="fa fa-circle-o"></i> Consultar Facturas</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Asignar Insumos a CA
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>admins/asignacion_insumos"><i class="fa fa-circle-o"></i> Nueva Asignacion</a></li>
                <li><a href="<?=base_url();?>admins/lista_asignaciones"><i class="fa fa-circle-o"></i> Consultar Asignaciones</a></li>
              </ul>
            </li>
            <li><a href="<?=base_url();?>admins/inventario_insumos"><i class="fa fa-circle-o"></i> Inventario</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Consultar Registros
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>admins/catalogo_insumos"><i class="fa fa-circle-o"></i>Catalogo</a></li>
                <li><a href="<?=base_url();?>admins/lista_insumos"><i class="fa fa-circle-o"></i> Insumos</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i>Notas
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/nota_entrega/index"><i class="fa fa-circle-o"></i>Entrega</a></li>
                <li><a href="<?=base_url();?>index.php/nota_transferencia/index"><i class="fa fa-circle-o"></i>Transferencia</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i>Reportes
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/reportes/index"><i class="fa fa-circle-o"></i>Reportes</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-desktop"></i> <span>Centro de Acopio</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i>Nota Recepcion
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/nota_recepcion/index"><i class="fa fa-circle-o"></i> Nota de Recepcion</a></li>
                <li><a href="<?=base_url();?>index.php/nota_recepcion/transferencias"><i class="fa fa-circle-o"></i>Recepcion de transf.</a></li>
              </ul>
            </li>
            <li>
              <a href="<?=base_url();?>index.php/despacho/index"><i class="fa fa-circle-o"></i>Transacciones
              </a>
            </li>
            <li><a href="<?=base_url();?>index.php/entregas/index"><i class="fa fa-circle-o"></i> Entregas Realizadas</a></li>
            <li><a href="<?=base_url();?>index.php/inventario_ca/index"><i class="fa fa-circle-o"></i> Inventario</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
<?php endif; ?>
  <div class="flash-data">
    <!-- CUANDO INICIA SESION -->
    <?php if($this->session->flashdata('user_loggedin')): ?>
        <?='<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>'?>
    <?php else: ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('jefe_id')): ?>
        <?='<p class="alert alert-danger">'.$this->session->flashdata('jefe_id').'</p>'?>
    <?php else: ?>
    <?php endif; ?>
    <!-- CUANDO FALLA EL INICIO DE SESION -->
    <?php if($this->session->flashdata('login_failed')): ?>
        <?='<p class="alert alert-danger" style="position: absolute; left: 70%; top: 150px;">'.$this->session->flashdata('login_failed').'</p>'?>
    <?php else: ?>
    <?php endif; ?>
    <!-- CUANDO LA FACTURA YA EXISTE -->
    <?php if($this->session->flashdata('receipt_exists')): ?>
        <?='<input type="hidden" id="receipt_exists" value="1">'?>
    <?php else: ?>
    <?php endif; ?>
  </div>
