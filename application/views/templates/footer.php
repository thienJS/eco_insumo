</div>
  <!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2018 <a href="#">Corporacion de Desarrollo Agricola DELAGRO</a>.</strong> Todos los derechos reservados.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script>
	var base_url='<?=base_url();?>';
</script>
<script src="<?=base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Scripts -->
<script src="<?=base_url();?>scripts/carga_insumo.js"></script>
<script src="<?=base_url();?>scripts/dependant_scripts.js"></script>
<script src="<?=base_url();?>scripts/check_dates.js"></script>
<script src="<?=base_url();?>scripts/ajax_modal.js"></script>
<script src="<?=base_url();?>scripts/peso_script.js"></script>
<script src="<?=base_url();?>scripts/num_validate.js"></script>
<script src="<?=base_url();?>scripts/confirm_form.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url();?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url();?>custom_assets/alerts/sweetalert.min.js"></script>
<!-- Select2 -->
<script src="<?=base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url();?>assets/bower_components/datatable/DataTable/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatable/DataTable/js/dataTables.bootstrap.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatable/Buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatable/Buttons/js/buttons.flash.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatable/JSZip/jszip.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatable/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatable/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatable/Buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatable/Buttons/js/buttons.print.min.js"></script>
<!-- Morris.js charts -->
<script src="<?=base_url();?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?=base_url();?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?=base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=base_url();?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=base_url();?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?=base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- AdminLTE for demo purposes -->

<!-- initialize select2 -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  });
</script>

<script src="<?=base_url();?>scripts/reportes.js"></script>

<script src="<?=base_url();?>scripts/transacciones.js"></script>
<!-- Datatables JS -->
<script src="<?=base_url();?>scripts/datatables/datatableInit.js"></script>

</body>
</html>
