<section class="content">
  <div class="box box-primary">
    <div class="box-header text-center nav-custom">
      <h2 class="box-title">RECEPCIONAR TRANSFERENCIA</h2>
    </div>
    <form action="" method="POST">
      <div id="box" class="box-body">
        <div class="row">
          <div class="form-group">
            <div class="col-sm-4">
                <label class="form-label">Nota de Transferencia</label>
                <input type="text" id="nota" class="form-control number_valid" name="nota" maxlength="7" value="<?= $nt_data[0]['nota_transferencia']; ?>" readonly>
            </div>
            <div class="col-sm-4">
                <label class="form-label">Centro acopio</label>
                <input type="text" id="nota" class="form-control number_valid" name="nota" maxlength="7" value="<?= $nt_data[0]['name']; ?>" readonly>
            </div>
            <div class="col-sm-4">
                <label class="form-label">Representante</label>
                <input type="text" id="nota" class="form-control number_valid" name="nota" maxlength="7" value="<?= $nt_data[0]['nombre']. ' '. $nt_data[0]['apellido']; ?>" readonly>
            </div>
          </div>
        </div>
      </div>
      <div class="box-header text-center nav-custom">
        <h2 class="box-title">DATOS DE INSUMO</h2>
      </div>
      <div id="box" class="box-body">
        <div class="row" id="myCol">
          <div class="form-group">
            <div class="col-sm-4">
              <label class="form-label">Tipo</label>
              <select class="form-control select2" id="s1">
                  <option value="0">--Seleccione Tipo de Insumo--</option>
                  <?php foreach($tipos as $tipo):?>
                          <option value="<?=$tipo['id'];?>"><?=$tipo['name'];?></option>
                  <?php endforeach;?>
              </select>
            </div>
            <div class="col-sm-4">
              <label class="form-label">Subtipo</label>
              <select class="form-control select2" id="s2">
                  <option value="0">--Seleccione Subtipo de Insumo--</option>
              </select>
            </div>
            <div class="col-sm-4">
              <label class="form-label">Insumo</label>
              <select class="form-control select2" id="s3">
                  <option value="0">--Seleccione Insumo--</option>
              </select>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="form-group">
            <div class="col-sm-4">
              <label class="form-label">Presentacion</label>
              <select class="form-control select2" id="s4">
                  <option value="0">--Seleccione Presentacion--</option>
              </select>
            </div>
            <div class="col-sm-4">
              <label class="form-label">Cantidad</label>
              <input type="text" class="form-control number_valid" id="cant" placeholder="Ingrese Cantidad">
            </div>
            <div class="col-sm-4">
              <label class="form-label">Peso (KG)</label>
              <input type="text" class="form-control" id="peso" placeholder="0" readonly>
            </div>
          </div>
        </div>
        <br>
        <div class="row text-center">
          <button type="button" id="myElement" class="btn btn-primary btn-circle waves-effect waves-circle waves-float">
            <i class="fa fa-lg fa-plus"></i>
          </button>
        </div>
        </div>
        <div class="box-body content-block table-responsive">
          <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>TIPO</th>
              <th>SUBTIPO</th>
              <th>INSUMO</th>
              <th>PRESENTACION</th>
              <th>CANTIDAD</th>
              <th>PESO</th>
              <th></th>
            </tr>
          </thead>
          <tbody id="myTable">

          </tbody>
          </table>
          <tfoot>
						<th colspan="4" style="visibility:hidden;"></th>
						<th>CANTIDAD TOTAL: <span id="cant-total">0</span></th>
						<th>PESO TOTAL: <span id="peso-total">0</span> <span>KG</span></th>
					</tfoot>
        <br>
        <div class="box-header text-center nav-custom">
        <h2 class="box-title">OBSERVACIONES</h2>
          </div>
      <div class="box-body">
          <div class="body table-responsive">
            <h4 class="card-inside-title">Por favor describa su Observacion...</h4>
            <div class="form-group">
              <div class="">
                  <textarea rows="4" class="form-control no-resize" ></textarea>
              </div>
            </div>
          </div>
          <div class="icon-and-text-button-demo">
        <button type="submit" id="mySubmit" class="btn btn-primary">
          <span>Guardar</span>
        </button>
      </div>
        </div>
      <br>

        </div>

      </div>
    </form>
  </div>
</section>