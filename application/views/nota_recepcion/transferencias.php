<section class="content">
  <div class="box-body content-block table-responsive">
    <div class="box-header text-center nav-custom">
      <h2 class="box-title">RECEPCIONAR TRANSFERENCIAS</h2>
    </div>
    <br>
    <?php if(empty($transferencias)) : ?>
      <h1 class="text-center">No posee transferencias Pendientes!</h1>

    <?php else: ?>
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>CENTRO ACOPIO</th>
          <th>REPRESENTANTE</th>
          <th>OBSERVACION</th>
          <th>CORRELATIVO</th>
          <th>STATUS</th>
          <th></th>
        </tr>
      </thead>
      <tbody id="myTable">
        <tr>
        <?php foreach($transferencias as $transferencia) : ?>
          <td><?= $transferencia['name']; ?></td>
          <td><?= $transferencia['nombre']. ' '. $transferencia['apellido']; ?></td>
          <td><?= $transferencia['observacion']; ?></td>
          <td><?= $transferencia['nota_transferencia']; ?></td>
          <td style="text-transform: uppercase; color: red"><strong><?= $transferencia['descripcion']; ?></strong></td>
          <td><a href="<?= base_url(); ?>index.php/nota_recepcion/r_transferencias/<?= $transferencia['id_nt']; ?>" class="btn btn-primary btn-block"><i class="fa fa-check"></a></td>
        <?php endforeach; ?>
        </tr>

      </tbody>
    </table>
    <?php endif; ?>

  </div>
</section>