<section class="content">
<div class="box">
        <div class="box-header nav-custom text-center">
            <h3 class="box-title"><?=$title?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="dt table table-striped table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>FECHA</th>
                        <th>FUERZA PRODUCTIVA</th>
                        <th>INSUMOS</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($entregas as $entrega): ?>
                    <tr>
                        <td><?=$entrega['id_nota_entrega']?></td>
                        <td><?=$entrega['fecha']?></td>
                        <td><?=$entrega['fp']?></td>
                        <td>
                        <button type="button" id="ins" class="btn" data-toggle="modal" data-target="#modal-default">
                            Ver
                        </button>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="modal fade" id="modal-default">
          <div class="modal-dialog" style="width:75%">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insumos Entregados</h4>
              </div>
              <div class="modal-body">
              <div class="box-body">
            <table class="table table-striped table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>TIPO INSUMOS</th>
                        <th>SUBTIPO INSUMO</th>
                        <th>INSUMO</th>
                        <th>CANTIDAD</th>
                        <th>PESO</th>
                    </tr>
                </thead>
                <tbody id="insTable">
                </tbody>
            </table>
        </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>

