<section class="content">
    <div class="box">
        <div class="box-header nav-custom text-center">
            <h3 class="box-title">REPORTE SEMAFORO</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="factura_table" class="dt table table-striped table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NUMERO DE ENTREGA</th>
                        <th>FECHA</th>
                        <th>ID CA</th>
                        <th>ID FP</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($entregas as $entrega): ?>
                    <tr>
                        <td><?=$entrega['id'];?></td>
                        <td><?=$entrega['nota_entrega'];?></td>
                        <td>
                            <?=$entrega['fecha'];?>
                            <input type="hidden" class="hidden_date" value="<?=$entrega['fecha']?>" disabled>
                            <input type="hidden" class="current_date" value="<?=date('Y-m-d')?>" disabled>
                        </td>
                        <td><?=$entrega['id_ca'];?></td>
                        <td><?=$entrega['id_fp'];?></td>
                        <td class="text-center semaforo"></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>