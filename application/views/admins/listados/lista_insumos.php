<section class="content">
    <div class="box">
        <div class="box-header nav-custom text-center">
            <h3 class="box-title">LISTA DE INSUMOS</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div>
                <button class="btn btn-success margin-dt" data-toggle="modal" data-target="#insumoModal">
                    <span><i class="fa fa-plus"></i></span> AGREGAR INSUMO
                </button>
            </div>
            <table id="linsumo_table" class="dt table table-striped table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tipo</th>
                        <th>Subtipo</th>
                        <th>Insumo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($linsumos as $linsumo): ?>
                    <tr>
                        <td><?=$linsumo['id'];?></td>
                        <td><?=$linsumo['tipo_insumo'];?></td>
                        <td><?=$linsumo['subtipo_insumo'];?></td>
                        <td><?=$linsumo['insumo'];?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>