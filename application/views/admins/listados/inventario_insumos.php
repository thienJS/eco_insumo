<section class="content">
    <div class="box">
        <div class="box-header nav-custom text-center">
            <h3 class="box-title">INVENTARIO DE INSUMOS</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="inventario_table" class="dt table table-striped table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tipo</th>
                        <th>Subtipo</th>
                        <th>Insumo</th>
                        <th>Presentacion</th>
                        <th>Tipo Presentacion</th>  
                        <th>Cantidad</th>
                        <th>Peso Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($inventarios as $inventario): ?>
                    <tr>
                        <td><?=$inventario['id']?></td>
                        <td><?=$inventario['tipo_insumo']?></td>
                        <td><?=$inventario['subtipo_insumo']?></td>
                        <td><?=$inventario['insumo']?></td>
                        <td><?=$inventario['presentacion']?></td>
                        <td><?=$inventario['tipo_presentacion']?></td>
                        <td><?=$inventario['cantidad']?></td>
                        <td><?=$inventario['peso']?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>