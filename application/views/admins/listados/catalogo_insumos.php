<section class="content">
    <div class="box">
        <div class="box-header nav-custom text-center">
            <h3 class="box-title">CATALOGO DE INSUMOS</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div>
                <button class="btn btn-success margin-dt" data-toggle="modal" data-target="#catalogoModal">
                    <span><i class="fa fa-plus"></i></span> AGREGAR INSUMO A CATALOGO
                </button>
            </div>
            <table id="catalogo_table" class="dt table table-striped table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tipo</th>
                        <th>Subtipo</th>
                        <th>Insumo</th>
                        <th>Presentacion</th>
                        <th>Tipo Presentacion</th>
                        <th>Peso Unitario</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($catalogos as $catalogo): ?>
                    <tr>
                        <td><?=$catalogo['id'];?></td>
                        <td><?=$catalogo['tipo_insumo'];?></td>
                        <td><?=$catalogo['subtipo_insumo'];?></td>
                        <td><?=$catalogo['insumo'];?></td>
                        <td><?=$catalogo['presentacion'];?></td>
                        <td><?=$catalogo['tipo_presentacion'];?></td>
                        <td><?=$catalogo['peso_unidad'];?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>