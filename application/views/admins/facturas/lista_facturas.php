<section class="content">
<?php date_default_timezone_set('America/Caracas'); ?>
<div class="box">
        <div class="box-header nav-custom text-center">
            <h3 class="box-title"><?=$title?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="factura_table" class="dt table table-striped table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th># FACTURA</th>
						<th>FECHA DE REGISTRO</th>
                        <th>PROVEEDOR</th>
						<th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($lista_facturas as $lista_factura): ?>
                    <tr>
                        <td><?=$lista_factura['id']?></td>
                        <td><?=$lista_factura['nro_factura']?></td>
						<td class="dates"><?=$lista_factura['fecha_registro']?></td>
                        <td><?=$lista_factura['proveedor']?></td>
                        <input type="hidden" class="hidden_date" value="<?=$lista_factura['date_registro']?>" disabled>
                        <input type="hidden" class="current_date" value="<?=date('Y-m-d')?>" disabled>
						<td class="text-center semaforo">
							<a class="factura" href="#" data-toggle="modal" data-target="#facturaModal" data-value="<?=$lista_factura['id']?>" title="Ver Insumos de la Factura">
                    			<span><i class="fa fa-lg fa-info-circle"></i></span>
							</a>
                            <span>-</span>
						</td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
