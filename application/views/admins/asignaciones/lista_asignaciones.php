<section class="content">
<div class="box">
        <div class="box-header nav-custom text-center">
            <h3 class="box-title"><?=$title?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="asignacion_table" class="dt table table-striped table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th># ASIGNACION</th>
                        <th>CENTRO DE ACOPIO</th>
                        <th>FECHA DE ASIGNACION</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($lista_asignaciones as $lista_asignacion): ?>
                    <tr>
                        <td><?=$lista_asignacion['id']?></td>
                        <td><?=$lista_asignacion['nro_asignacion']?></td>
                        <td><?=$lista_asignacion['name']?></td>
                        <td><?=$lista_asignacion['fecha_asignacion']?></td>
                        <td class="text-center semaforo">
							<a class="asignacion" href="#" data-toggle="modal" data-target="#asignacionModal" data-value="<?=$lista_asignacion['id']?>" title="Ver insumos de la Asignacion">
                    			<span><i class="fa fa-lg fa-info-circle"></i></span>
							</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>