<section class="content">
    <?= validation_errors(); ?>
    <form class="confirm-form" action="asignacion_insumos" method="POST">
		<div class="box box-primary">
			<div class="box-header text-center nav-custom">
				<h1 class="box-title"><?=$title?></h1>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="form-group">
						<div class="col-sm-6">
							<label class="form-label">Fecha</label>
							<input type="text" class="form-control" value="<?=date('d-m-Y')?>" disabled>
						</div>
						<div class="col-sm-6">
							<label class="form-label">Numero de Asignacion</label>
							<input type="text" class="form-control number_valid" name="nro_asignacion" maxlength="8" id="nro_documento">
						</div>
					</div>
				</div>
			</div>
			<div class="box-header text-center nav-custom">
				<h2 class="box-title">DATOS DE CENTRO DE ACOPIO</h2>
			</div>
			<div class="box-body">
				<div class="row" id="myCol">
					<div class="form-group">
						<div class="col-sm-6">
							<label class="form-label">Estado</label>
							<select class="form-control select2" id="e1">
								<option value="0">--Seleccione estado--</option>
								<?php foreach($estados as $estado):?>
										<option value="<?=$estado['id'];?>"><?=$estado['descripcion'];?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="col-sm-6">
							<label class="form-label">Centro de Acopio</label>
							<select name="centro_a" class="form-control select2" id="e2">
								<option value="0">--Seleccione Centro de Acopio--</option>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="form-group">
						<div class="col-sm-6">
							<label class="form-label">Municipio</label>
							<input class="form-control" type="text" name="municipio" id="municipio" disabled>
						</div>
						<div class="col-sm-6">
							<label>Parroquia</label>
							<input class="form-control" type="text" name="parroquia" id="parroquia" disabled>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label>Direccion</label>
							<textarea name="direccion" id="direccion" rows="3" class="form-control" disabled></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="box-header text-center nav-custom">
				<h2 class="box-title">DATOS DE INSUMO</h2>
			</div>
			<div class="box-body">
				<div class="row" id="myCol">
					<div class="form-group">
						<div class="col-sm-4">
							<label class="form-label">Tipo</label>
							<select class="form-control select2" id="s1">
								<option value="0">--Seleccione Tipo de Insumo--</option>
								<?php foreach($tipos as $tipo):?>
										<option value="<?=$tipo['id'];?>"><?=$tipo['name'];?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="col-sm-4">
							<label class="form-label">Subtipo</label>
							<select class="form-control select2" id="s2">
								<option value="0">--Seleccione Subtipo de Insumo--</option>
							</select>
						</div>
						<div class="col-sm-4">
							<label class="form-label">Insumo</label>
							<select class="form-control select2" id="s3">
								<option value="0">--Seleccione Insumo--</option>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="form-group">
						<div class="col-sm-3">
							<label class="form-label">Presentacion</label>
							<select class="form-control select2" id="s4">
								<option value="0">--Seleccione Presentacion--</option>
							</select>
						</div>
						<div class="col-sm-3">
							<label class="form-label">Inventario</label>
							<input type="text" class="form-control" id="inventario" placeholder="0" disabled>
						</div>
						<div class="col-sm-3">
							<label class="form-label">Cantidad</label>
							<input type="text" class="form-control number_valid" id="cant" placeholder="Ingrese Cantidad" disabled>
						</div>
						<div class="col-sm-3">
							<label class="form-label">Peso (KG)</label>
							<input type="text" class="form-control" id="peso" placeholder="0" disabled	>
						</div>
					</div>
				</div>
				<br>
				<div class="row text-center">
					<button type="button" id="myElement" class="btn btn-primary btn-circle waves-effect waves-circle waves-float ">
						<i class="fa fa-lg fa-plus"></i>
					</button>
				</div>
			</div>
			<div class="box-body content-block table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>TIPO</th>
							<th>SUBTIPO</th>
							<th>INSUMO</th>
							<th>PRESENTACION</th>
							<th>CANTIDAD</th>
							<th>PESO</th>
							<th></th>
						</tr>
					</thead>
					<tbody id="myTable">

					</tbody>
					<tfoot>
						<th colspan="4" style="visibility:hidden;"></th>
						<th>CANTIDAD TOTAL: <span id="cant-total">0</span></th>
						<th>PESO TOTAL: <span id="peso-total">0</span> <span>KG</span></th>
					</tfoot>
				</table>
				<br>
			</div>
			<div class="box-footer text-center" >
				<button type="submit" id="mySubmit" class="btn btn-success btn-circle ">
					<i class="fa fa-lg fa-check"></i>
				</button>
			</div>
		</div>
    </form> 
</section>
