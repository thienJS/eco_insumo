<section class="content">
    <form action="" method="POST">
		<div class="box box-primary">
			<div class="box-header text-center nav-custom">
				<h1 class="box-title"><?=$title?></h1>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="form-group">
						<div class="col-sm-6">
								<label class="form-label" >Nota de Entrega</label>
								<input id="nota" name="nota" type="text" class="form-control number_valid" value="" maxlength="7" >
						</div>
						<div class="col-sm-6">
								<label class="form-label">Fecha</label>
								<input type="text" class="form-control" value="<?=date('d-m-Y')?>" readonly>
						</div>
					</div>
				</div>
			</div>
			<div class="box-header text-center nav-custom">
				<h2 class="box-title">DATOS DE CENTRO ACOPIO</h2>
			</div>
				<div class="box-body">
					<div class="row">
						<div class="form-group">
							<div class="col-sm-12">
								<label class="form-label">Centro Acopio</label>
								<input type="text" class="form-control" value="<?= $ca[0]['name']; ?>" readonly>
								<input type="text" name="ca" value="1" hidden>
							</div>
						</div>
					</div>
				</div>
				<div class="box-header text-center nav-custom">
					<h2 class="box-title">DATOS DEL DESTINO</h2>
				</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group">
								<div class="col-sm-6">
									<label class="form-label">Estado</label>
									<select class="form-control" id="e">
										<option value="0">--Seleccione Tipo de Insumo--</option>
										<?php foreach($estados as $estado) : ?>
										<option value="<?=$estado['id'];?>"><?=$estado['estados'];?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-sm-6">
									<label class="form-label">Fuerza Productiva</label>
									<select class="form-control" id="fp" name="fp">
										<option value="0">--Seleccione Tipo de Insumo--</option>
									</select>
								</div>
								<!-- <div class="col-sm-3">
									<label class="form-label">Municipio</label>
									<select class="form-control">
										<option value="0">--Seleccione Tipo de Insumo--</option>
									</select>
								</div>
								<div class="col-sm-3">
									<label class="form-label">Parroquia</label>
									<select class="form-control">
										<option value="0">--Seleccione Tipo de Insumo--</option>
									</select>
								</div> -->
							</div>
						</div>
					</div>
			<div class="box-header text-center nav-custom">
				<h2 class="box-title">DATOS DE INSUMO</h2>
			</div>
			<div class="box-body">
				<div class="row" id="myCol">
					<div class="form-group">
						<div class="col-sm-4">
							<label class="form-label">Tipo</label>
							<select class="form-control select2" id="s1">
								<option>--Seleccione Tipo de Insumo--</option>
								<?php foreach($tipos as $tipo):?>
										<option value="<?=$tipo['id'];?>"><?=$tipo['name'];?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="col-sm-4">
							<label class="form-label">Subtipo</label>
							<select class="form-control select2" id="s2">
								<option value="0">--Seleccione Subtipo de Insumo--</option>
							</select>
						</div>
						<div class="col-sm-4">
							<label class="form-label">Insumo</label>
							<select class="form-control select2" id="s3">
								<option value="0">--Seleccione Insumo--</option>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="form-group">
						<div class="col-sm-3">
							<label class="form-label">Presentacion</label>
							<select class="form-control select2" id="s4">
								<option value="0">--Seleccione Presentacion--</option>
							</select>
						</div>
						<div class="col-sm-3">
							<label class="form-label">Inventario</label>
							<input type="text" class="form-control" id="inventario_ca" placeholder="0" readonly>
						</div>
						<div class="col-sm-3">
							<label class="form-label">Cantidad</label>
							<input type="text" class="form-control number_valid" id="cant" placeholder="Ingrese Cantidad">
						</div>
						<div class="col-sm-3">
							<label class="form-label">Peso (KG)</label>
							<input type="text" class="form-control" id="peso" placeholder="0" readonly>
						</div>
					</div>
				</div>
				<br>
				<div class="row text-center">
					<button type="button" id="myElement" class="btn btn-primary btn-circle waves-effect waves-circle waves-float ">
						<i class="fa fa-lg fa-plus"></i>
					</button>
				</div>
			</div>
			<div class="box-body content-block table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>TIPO</th>
							<th>SUBTIPO</th>
							<th>INSUMO</th>
							<th>PRESENTACION</th>
							<th>CANTIDAD</th>
							<th>PESO</th>
							<th></th>
						</tr>
					</thead>
					<tbody id="myTable">

					</tbody>
				</table>
				<br>
				</div>
			<div class="box-footer text-center" >
			</div>
					<div class="box-header text-center nav-custom">
						<h2 class="box-title">OBSERVACIONES</h2>
			</div>
			<div class="box-body">
				<div class="body table-responsive">
					<h4 class="card-inside-title">Por favor describa su Observacion...</h4>
					<div class="form-group">
						<div class="">
								<textarea rows="4" class="form-control no-resize" ></textarea>
						</div>
					</div>
				</div>
			</div>
					<br>
					<div class="box-header text-center nav-custom">
						<h2 class="box-title">INFORMACION ADICIONAL</h2>
			</div>
					<div class="box-body">
						<div class="row">
							<div class="col-sm-12">
									<!-- Nav tabs -->
									<ul class="nav nav-tabs tab-nav-right" role="tablist">
										<li role="presentation" class="active"><a href="#home_animation_2" data-toggle="tab">DATOS DEL TRANSPORTISTA</a></li>
										<li role="presentation" ><a  href="#item_animation_2" data-toggle="tab">DATOS DEL REPRESENTANTE</a></li>
										<li role="presentation"><a href="#profile_animation_2" data-toggle="tab">DATOS DEL JEFE DEL CENTRO DE ACOPIO</a></li>

									</ul>
									<!-- Tab panes -->
									<div class="tab-content">
										<div role="tabpanel" class="tab-pane animated fadeInRight active" id="home_animation_2">
											<br>
											<div class="row">
								<div class="col-sm-2">
									<b>Cedula</b>
									<div class="input-group">
										<div class="">
											<input type="text" name="ci" maxlength="9" id="ci1" class="number_valid form-control" >
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<b>Nombre</b>
									<div class="form-group">
										<div class="">
											<input type="text" name="nombres" id="nam" class="form-control">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<b>Apellido</b>
									<div class="form-group">
										<div class="">
											<input type="text" name="apellidos" id="lastn" class="form-control">
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<b>Telefono</b>
									<div class="input-group">
										<div class="">
											<input type="text" name="telefono" id="tlf1" class="number_valid form-control">
										</div>
									</div>
								</div>
								<br><br><br>
								<div class="col-md-12">
									<h4><strong>DATOS DEL VEHICULO</strong></h4>
								</div>
								<br>
								<div class="col-sm-4">
									<label for="">Empresa para quien trabaja</label>
									<input type="text" name="empresa" class="form-control" id="empresa" value="">
								</div>
								<div class="col-md-4">
									<label for="">Tipo Vehiculo</label>
									<input type="text" name="tvehiculo" class="form-control" id="tvh" value="" >
								</div>
								<div class="col-md-4">
									<label for="">Placa</label>
									<input type="text" name="placa" class="form-control" id="placa" maxlength="7" value="">
								</div>
							</div>
										</div>
										<div role="tabpanel" class="tab-pane animated fadeInRight" id="item_animation_2">
											<div class="row">
												<div class="col-sm-4">
													<label for="">Nombre y Apellido</label>
													<input type="text" name="" id="names" class="form-control" value="" readonly>
												</div>
												<div class="col-sm-4">
													<label for="">Cedula</label>
													<input type="text" class="form-control number_valid" value="" name="" id="ci" readonly>
												</div>
												<div class="col-sm-4">
												<label for="">telefono</label>
												<input type="text" name="" readonly class="form-control number_valid" value="" id="tlf">
												</div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">
										<div class="row">
											<div class="col-sm-4">
												<label for="">Nombre y Apellido</label>
												<input type="text" class="form-control" value="<?=$sessionJefe[0]['nombre']. ' '.$sessionJefe[0]['apellido']; ?>" readonly>
											</div>
											<div class="col-sm-4">
												<label for="">Cedula</label>
												<input type="text" class="form-control number_valid" value="<?=$sessionJefe[0]['cedula']; ?>" readonly>
											</div>
											<div class="col-sm-4">
											<label for="">telefono</label>
											<input type="text" class="form-control number_valid" value="<?=$sessionJefe[0]['telefono']; ?>" readonly>
											</div>
										</div>
										</div>
									</div>
							</div>
						</div>
						<br>
						<div class="icon-and-text-button-demo">
							<button type="submit" id="mySubmit" class="btn btn-primary">
								<span>Guardar</span>
							</button>
						</div>
					</div>
		</div>
   </form>
</section>
