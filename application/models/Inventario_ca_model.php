<?php
    class Inventario_ca_model extends CI_Model{
      //Consulta los Registros del Inventario
      public function get_inventario(){
          $this->db->select(' cat.id,
                              ti.name as tipo,
                              st.name as subtipo,
                              ins.name as insumo,
                              cat.presentacion,
                              inv.cantidad,
                              inv.peso');
        //   $this->db->join('nota_recepcion_x_insumos as nr','inv.id_nr_x_ins = nr.id_nr_x_ins','left');
          $this->db->join('catalogo_insumos as cat','inv.id_catalogo = cat.id','left');
          $this->db->join('tipos_insumo as ti','cat.id_tipo_insumo = ti.id','left');
          $this->db->join('subtipos_insumo as st','cat.id_subtipo_insumo = st.id','left');
          $this->db->join('insumos as ins','cat.id_insumo = ins.id','left');
          $this->db->where('inv.id_ca', $this->session->ca_id);
          $query = $this->db->get('inventario_ca as inv');
          return $query->result_array();
      }

        public function ca_x_catalogo($ca, $cat){
            $this->db->select('id_catalogo');
            $this->db->where('id_ca', $ca);
            $this->db->where('id_catalogo', $cat);
            $q = $this->db->get('inventario_ca');
            return $this->db->affected_rows();
        }

        // Suma al inventario
        public function update_inventario_ca($ca, $i, $cant, $pes){

            $cantidad = $this->input->post('cantidad');
            $peso = $this->input->post('peso');
            // $c_p = $this->select_cant_peso(1, $catalogo, $i);
            $tCantidad = intval($cantidad[$i]) + $cant;
            $tPeso = floatval($peso[$i]) + $pes;
            $catalogo = $this->input->post('catalogo');

            $data = array(
                'cantidad' => $tCantidad,
                'peso' => $tPeso
            );

            $this->db->where('id_ca', $ca[0]);
            $this->db->where('id_catalogo', $catalogo[$i]);
			return $this->db->update('inventario_ca', $data);
        }

        public function select_cant_peso($ca, $cat, $i){
            $this->db->select('cantidad,
            peso');
            $this->db->where('id_ca', $ca);
            $this->db->where('id_catalogo', $cat[$i]);
            $query = $this->db->get('inventario_ca');
            return $query->result_array();
        }

        // resta al inventario
        public function update_inventario_ca_minus($ca, $i, $cant, $pes){
            $cantidad = $this->input->post('cantidad');
            $peso = $this->input->post('peso');
            // $c_p = $this->select_cant_peso(1, $catalogo, $i);
            $tCantidad = intval($cant -$cantidad[$i]) ;
            $tPeso = floatval($pes - $peso[$i]) ;
            $catalogo = $this->input->post('catalogo');

            $data = array(
                'cantidad' => $tCantidad,
                'peso' => $tPeso
            );

            $this->db->where('id_ca', $ca[0]);
            $this->db->where('id_catalogo', $catalogo[$i]);
			return $this->db->update('inventario_ca', $data);
        }

        public function insert_inventario($ca, $cat){
            $data = array(
                'id_ca' => $ca,
                'id_catalogo' => $cat,
                'cantidad' => 0,
                'peso' => 0
            );

            $this->db->insert('inventario_ca', $data);
        }
    }
?>