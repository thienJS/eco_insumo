<?php
  class Nota_transferencia_model extends CI_Model{
    public function get_representante($postData){

      $this->db->select(' p.first_name as nombre,
                          p.first_lastname as apellido,
                          p.cedula,
                          p.telefono');
      $this->db->join('jefes_ca as j', 'c.id_jefe_ca = j.id', 'left');
      $this->db->join('personas as p', 'j.id_persona = p.id', 'left');
      $this->db->where('c.id', $postData['this_ca']);
      $query = $this->db->get('centros_acopio as c');
      $response = $query->result_array();
      return $response;
    }


    public function get_current_ca($jefeId){
      $this->db->select(' c.name as ca,
                          c.id,
                          p.first_name as nombre,
                          p.first_lastname as apellido,
                          p.telefono,
                          p.cedula');
      $this->db->join('jefes_ca as j', 'c.id_jefe_ca = j.id', 'left');
      $this->db->join('personas as p', 'j.id_persona = p.id', 'left');
      $this->db->where('j.id', $jefeId);
      $query = $this->db->get('centros_acopio as c');
      $response = $query->result_array();
      return $response;
    }

    public function create_nota_transferencia($idTransportista){
      $data = array(
        'nota_transferencia' => $this->input->post('nota'),
        'observacion' => $this->input->post('observacion'),
        'id_ca' => $this->session->ca_id,
        'id_ca_destino' => $this->input->post('ca_destino'),
        'id_transportista' => $idTransportista,
        'id_status' => 3
      );

      $q = $this->db->insert('nota_transferencia', $data);

      return $this->db->insert_id();
    }

    public function create_nota_t_x_catalogo($idNota, $i){
      $catalogo = $this->input->post('catalogo');
      $cantidad = $this->input->post('cantidad');
      $peso = $this->input->post('peso');

      $data = array(
        'id_nota_transferencia' => $idNota,
        'cantidad' => $cantidad[$i],
        'peso_cantidad' => $peso[$i],
        'id_catalogo' => $catalogo[$i]
      );

      $this->db->insert('nota_transferencia_x_insumos', $data);
    }

    public function create_recepcion_transferencia($idNt){
      $data = array(
        'observacion' => $this->input->post('observacion'),
        'id_ca' => $this->session->ca_id,
        'id_nota_transferencia' => $idNt
      );

      $this->db->insert('recepcion_transferencias', $data);
    }

    public function update_nt_status($idNt){
      $data = array(
        'id_status' => 4
      );
      // $this->db->set('id_status', 4);
      // $this->db->where('id_ca_destino', $this->session->ca_id);
      $this->db->where('id_nota_transferencia', $idNt);
      return $this->db->update('nota_transferencia', $data);
    }
  }
?>