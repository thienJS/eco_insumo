<?php
    class Lista_insumos_model extends CI_Model{
        //Consulta los Registros de Insumos
        public function get_lista_insumos(){
            $this->db->select(  'insumos.id as id,
                                tipos_insumo.name as tipo_insumo,
                                subtipos_insumo.name as subtipo_insumo,
                                insumos.name as insumo');
            $this->db->join('tipos_insumo','tipos_insumo.id = insumos.id_tipo_insumo','left');
            $this->db->join('subtipos_insumo','subtipos_insumo.id = insumos.id_subtipo_insumo','left');
            $query = $this->db->get('insumos');
            return $query->result_array();
        }

        public function create_insumo(){
            $data = array(
                'id_tipo_insumo' => $this->input->post('id_tipo_insumo'),
                'id_subtipo_insumo' => $this->input->post('id_subtipo_insumo'),
                'name' => $this->input->post('insumo')
            );

            return $this->db->insert('insumos',$data);
        }
    }
?>