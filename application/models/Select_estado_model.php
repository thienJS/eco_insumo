<?php
    class Select_estado_model extends CI_Model{
        //Consulta los Estados
        public function get_estados(){
            $response = array();
            $this->db->distinct('estados.descripcion, estados.id');
            $this->db->select('estados.descripcion, estados.id');
            $this->db->join('estados','centros_acopio.id_estado = estados.id');
            $this->db->order_by('estados.descripcion');
            $q = $this->db->get('centros_acopio');
            $response = $q->result_array();
            return $response;
        }

        //Consulta los Centros de Acopio
        public function get_ca($postData){
            $this->db->select('id,name');
            $this->db->where('id_estado',$postData['this_estado']);
            $this->db->order_by('name');
            $q = $this->db->get('centros_acopio');

            $response = $q->result_array();
            return $response;
        }

        //Consulta los Municipios del centro de acopio
        public function get_municipios($postData){

            $this->db->select('municipios.descripcion as municipio');
            $this->db->join('municipios','centros_acopio.id_municipio = municipios.id','left');
            $q = $this->db->get_where('centros_acopio',array('centros_acopio.id'=>$postData['this_ca']));
            $response = $q->row_array();
            return $response;
        }
        //Consulta las Parroquias del Centro de Acopio
        public function get_parroquias($postData){

            $this->db->select('parroquias.descripcion as parroquia');
            $this->db->join('parroquias','centros_acopio.id_parroquia = parroquias.id','left');
            $q = $this->db->get_where('centros_acopio',array('centros_acopio.id'=>$postData['this_ca']));
            $response = $q->row_array();
            return $response;
        }
        //Consulta la direccion del centro de acopio
        public function get_direccion($postData){

            $this->db->select('direccion');
            $q = $this->db->get_where('centros_acopio',array('id'=>$postData['this_ca']));
            $response = $q->row_array();
            return $response;
        }
    }
?>
