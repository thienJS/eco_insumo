<?php
  class Reporte_model extends CI_Model{
    //Consulta los Registros del Inventario
      public function agroquimicos(){
        $this->db->select('count(distinct id_insumo) as insumos');
        $this->db->where('id_tipo_insumo', 1);
        $query = $this->db->get('catalogo_insumos');
        return $query->result_array();
      }

      public function fertilizantes(){
        $this->db->select('count(distinct id_insumo) as insumos');
        $this->db->where('id_tipo_insumo', 2);
        $query = $this->db->get('catalogo_insumos');
        return $query->result_array();
      }

      public function semillas(){
        $this->db->select('count(distinct id_insumo) as insumos');
        $this->db->where('id_tipo_insumo', 3);
        $query = $this->db->get('catalogo_insumos');
        return $query->result_array();
      }
  }
?>