<?php
    class Inventario_model extends CI_Model{
        //Consulta los Registros del Inventario
        public function get_inventario(){
            $this->db->select(  'inventario.id_catalogo as id,
                                tipos_insumo.name as tipo_insumo,
                                subtipos_insumo.name as subtipo_insumo,
                                insumos.name as insumo,
                                catalogo_insumos.presentacion,
                                tipo_presentaciones.name as tipo_presentacion,
                                cantidad,
                                peso');
            $this->db->join('catalogo_insumos','catalogo_insumos.id = inventario.id_catalogo','left');
            $this->db->join('insumos','insumos.id = catalogo_insumos.id_insumo','left');
            $this->db->join('tipos_insumo','tipos_insumo.id = insumos.id_tipo_insumo','left');
            $this->db->join('subtipos_insumo','subtipos_insumo.id = insumos.id_subtipo_insumo','left');
            $this->db->join('tipo_presentaciones','tipo_presentaciones.id = catalogo_insumos.id_tipo_presentacion','left');
            $query = $this->db->get('inventario');
            return $query->result_array();
        }
    }
?>