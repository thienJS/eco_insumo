<?php
  class Entrega_model extends CI_Model{

    public function getIns($id){
      $this->db->select(' nei.cantidad,
                          nei.peso_cantidad,
                          ti.name as tipo_insumo,
                          sti.name as subtipo_insumo,
                          i.name as insumo');
      $this->db->join('catalogo_insumos as ci', 'nei.id_catalogo = ci.id', 'left');
      $this->db->join('tipos_insumo as ti', 'ci.id_tipo_insumo = ti.id', 'left');
      $this->db->join('subtipos_insumo as sti', 'ci.id_subtipo_insumo = sti.id', 'left');
      $this->db->join('insumos as i', 'ci.id_insumo = i.id', 'left');
      $this->db->where('nei.id_nota_entrega', $id);
      $query = $this->db->get('nota_entrega_x_insumos as nei');
      $response = $query->result_array();
      return $response;
    }

    public function entregas($type){
      $this->db->select('ne.id_nota_entrega,
      ne.nota_entrega,
      ne.fecha,
      fp.representante as fp');
      $this->db->join('fuerzas_productivas as fp', 'ne.id_fp = fp.id_fp', 'left');
      $this->db->where('id_ca', $this->session->ca_id);
      $this->db->where('status', $type);
      $query = $this->db->get('nota_entrega as ne');
      return $query->result_array();
    }
  }