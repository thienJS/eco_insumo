<?php
  class Nota_entrega_model extends CI_Model{

    public function get_fuerzasp_estados(){
      $this->db->distinct('e.descripcion as estados');
      $this->db->select('e.descripcion as estados, e.id');
      $this->db->join('estados as e', 'fp.id_estado = e.id', 'left');
      $query = $this->db->get('fuerzas_productivas as fp');
      $response = $query->result_array();
      return $response;
    }

    public function get_fp($postData){
      $this->db->select('id_fp, representante');
      $this->db->join('estados as e', 'id_estado = e.id', 'left');
      $this->db->where('e.id', $postData['this_estado']);
      $query = $this->db->get('fuerzas_productivas');
      $response = $query->result_array();
      return $response;
    }

    public function get_fp_representante($postData){
      $this->db->select('representante, rif_cedula, telefono');
      $this->db->where('id_fp', $postData['this_fp']);
      $query = $this->db->get('fuerzas_productivas');
      $response = $query->result_array();
      return $response;
    }

    public function create_nota_entrega($idTransportista){
      $data = array(
        'nota_entrega' => $this->input->post('nota'),
        'id_ca' => $this->session->ca_id,
        'id_fp' => $this->input->post('fp'),
        'id_transportista' => $idTransportista,
        'status' => 6
      );

      $q = $this->db->insert('nota_entrega', $data);

      return $this->db->insert_id();
    }

    public function create_nota_e_x_catalogo($idNota, $i){
      $catalogo = $this->input->post('catalogo');
      $cantidad = $this->input->post('cantidad');
      $peso = $this->input->post('peso');

      $data = array(
        'cantidad' => $cantidad[$i],
        'peso_cantidad' => $peso[$i],
        'id_nota_entrega' => $idNota,
        'id_catalogo' => $catalogo[$i]
      );

      $this->db->insert('nota_entrega_x_insumos', $data);
    }
  }
?>