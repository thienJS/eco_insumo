<?php
    class Catalogo_model extends CI_Model{
        //Consulta los Registros del Inventario
        public function get_catalogo(){
            $this->db->select(  'catalogo_insumos.id as id,
                                tipos_insumo.name as tipo_insumo,
                                subtipos_insumo.name as subtipo_insumo,
                                insumos.name as insumo,
                                catalogo_insumos.presentacion,
                                tipo_presentaciones.name as tipo_presentacion,
                                catalogo_insumos.peso_unidad as peso_unidad'
                            );
            $this->db->join('insumos','insumos.id = catalogo_insumos.id_insumo','left');
            $this->db->join('tipos_insumo','tipos_insumo.id = insumos.id_tipo_insumo','left');
            $this->db->join('subtipos_insumo','subtipos_insumo.id = insumos.id_subtipo_insumo','left');
            $this->db->join('tipo_presentaciones','tipo_presentaciones.id = catalogo_insumos.id_tipo_presentacion','left');
            $query = $this->db->get('catalogo_insumos');
            return $query->result_array();
        }

        //Nuevo registro en catalogo
        public function create_catalogo(){
            $data = array(
                'id_tipo_insumo' => $this->input->post('id_tipo_insumo'),
                'id_subtipo_insumo' => $this->input->post('id_subtipo_insumo'),
                'id_insumo' => $this->input->post('id_insumo'),
                'presentacion' => $this->input->post('presentacion'),
                'id_tipo_presentacion' => $this->input->post('id_tipo_presentacion'),
                'peso_unidad' => $this->input->post('peso_unidad')
            );

            $q = $this->db->insert('catalogo_insumos',$data);

            return $this->db->insert_id();
        }
    }
?>