<?php
  class Nota_recepcion_model extends CI_Model{
    public function get_ca($ca){
      $this->db->select('
        e.descripcion as estado,
        m.descripcion as municipio,
        p.descripcion as parroquia,
        c.id,
        c.name,
        c.direccion,
        per.first_name,
        per.first_lastname,
        per.telefono,
        per.cedula');
      $this->db->join('estados as e', 'c.id_estado = e.id', 'left');
      $this->db->join('municipios as m', 'c.id_municipio = m.id', 'left');
      $this->db->join('parroquias as p', 'c.id_parroquia = p.id', 'left');
      $this->db->join('jefes_ca as j', 'c.id_jefe_ca = j.id', 'left');
      $this->db->join('personas as per', 'j.id_persona = per.id', 'left');
      $this->db->where('c.id', $ca);
      $query = $this->db->get('centros_acopio as c');
      $response = $query->result_array();
      return $response;
    }

    public function create_vehiculo(){
      $data = array(
        'empresa' => $this->input->post('empresa'),
        'placa' => $this->input->post('placa'),
        'tipo_vehiculo' => $this->input->post('tvehiculo')
      );

      $q = $this->db->insert('vehiculos', $data);
      return $this->db->insert_id();
    }

    public function create_transportista($idVehiculo){
      $data = array(
        'nombres' => $this->input->post('nombres'),
        'apellidos' => $this->input->post('apellidos'),
        'cedula' => $this->input->post('ci'),
        'telefono' => $this->input->post('telefono'),
        'id_vehiculo' => $idVehiculo
      );

      $q = $this->db->insert('transportistas', $data);
      return $this->db->insert_id();
    }

    public function create_nota_recepcion($idTrasnportista){
      $data = array(
        'nota_recepcion' => $this->input->post('nota'),
        'id_ca' => $this->session->ca_id,
        'id_transportista' => $idTrasnportista,
        'id_asignacion' => $this->input->post('asignacion'),
        'observacion' => $this->input->post('observacion')
      );

      $q = $this->db->insert('nota_recepcion', $data);

      return $this->db->insert_id();
    }

    public function create_notaxcatalogo($idNota, $i){
      $catalogo = $this->input->post('catalogo');
      $cantidad = $this->input->post('cantidad');
      $peso = $this->input->post('peso');

      $data = array(
        'id_nota_recepcion' => $idNota,
        'id_catalogo' => $catalogo[$i],
        'cantidad' => $cantidad[$i],
        'peso_cantidad' => $peso[$i],
        'id_ca' => $this->session->ca_id
      );

      $this->db->insert('nota_recepcion_x_insumos', $data);
    }

    public function get_transferencias_pendientes(){
      $this->db->select(' nt.id_ca as id_ca,
      nt.id_nota_transferencia as id_nt,
      nt.nota_transferencia,
      c.name,
      nt.observacion,
      s.descripcion,
      p.first_name as nombre,
      p.first_lastname as apellido');
      $this->db->join('centros_acopio as c', 'c.id = nt.id_ca', 'left');
      $this->db->join('status as s', 'nt.id_status = s.id', 'left');
      $this->db->join('jefes_ca as j', 'c.id_jefe_ca = j.id', 'left');
      $this->db->join('personas as p', 'p.id = j.id_persona', 'left');
      $this->db->where('nt.id_ca_destino', $this->session->ca_id);
      $this->db->where('s.id', 3);
      $query = $this->db->get('nota_transferencia as nt');
      return $query->result_array();
    }

    public function get_asignaciones($ca){
      $this->db->select('id, nro_asignacion');
      $this->db->where('id_ca', $ca);
      $this->db->where('id_status', 3);
      $query = $this->db->get('asignaciones_ca');
      return $query->result_array();
    }

    public function get_ids_ca(){
      $this->db->select('id');
      $query = $this->db->get('centros_acopio');
      return $query->result_array();
    }

    public function get_ids_cat(){
      $this->db->select('id');
      $query = $this->db->get('catalogo_insumos');
      return $query->result_array();
    }

    public function update_asignacion(){
      $asignacion = $this->input->post('asignacion');

      $data = array(
        'id_status' => 4
      );

      $this->db->where('id', $asignacion);
      return $this->db->update('asignaciones_ca', $data);
    }

    public function get_transferencias_data($id){
      $this->db->select(' nt.id_ca as id_ca,
      nt.nota_transferencia,
      c.name,
      p.first_name as nombre,
      p.first_lastname as apellido');
      $this->db->join('centros_acopio as c', 'c.id = nt.id_ca', 'left');
      $this->db->join('jefes_ca as j', 'c.id_jefe_ca = j.id', 'left');
      $this->db->join('personas as p', 'p.id = j.id_persona', 'left');
      $this->db->where('nt.id_nota_transferencia', $id);
      $query = $this->db->get('nota_transferencia as nt');
      return $query->result_array();
    }
  }

?>