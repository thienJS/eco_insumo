<?php
    class Lista_facturas_model extends CI_Model{
        //
        public function get_lista_facturas(){
			$this->db->select('	facturas.id, 
								facturas.nro_factura, 
								facturas.fecha_registro, 
								date(facturas.fecha_registro) as date_registro, 
								proveedores.descripcion as proveedor');
			$this->db->join('proveedores','facturas.id_proveedor = proveedores.id');
            $query = $this->db->get('facturas');
            return $query->result_array();
		}
		
		public function get_insumos_factura($postData){
			$this->db->select(	'facturas.nro_factura as nro_factura,
								insumos_factura.id_catalogo as id_catalogo,
								insumos.name as name,
								tipos_insumo.name as tipo_insumo,
								subtipos_insumo.name as subtipo_insumo,
								catalogo_insumos.presentacion as presentacion,
								insumos_factura.cantidad as cantidad,
								insumos_factura.peso_cantidad as peso'
							);
			$this->db->join('facturas','insumos_factura.id_factura = facturas.id');
			$this->db->join('catalogo_insumos','insumos_factura.id_catalogo = catalogo_insumos.id','left');
			$this->db->join('insumos','catalogo_insumos.id_insumo = insumos.id','left');
			$this->db->join('tipos_insumo','insumos.id_tipo_insumo = tipos_insumo.id','left');
			$this->db->join('subtipos_insumo','insumos.id_subtipo_insumo = subtipos_insumo.id','left');
			$this->db->where('insumos_factura.id_factura',$postData['this_factura']);

			$query = $this->db->get('insumos_factura');

			return $query->result_array();
		}
    }
?>
