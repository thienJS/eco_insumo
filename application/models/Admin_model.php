<?php
    class Admin_model extends CI_Model{

        //CREA FACTURA DESPUES DE MANDAR EL FORM
        public function crear_factura(){
            $data = array(
				'nro_factura' => $this->input->post('nro_factura'),
				'id_proveedor' => $this->input->post('proveedor')
            );

            $this->db->select('nro_factura, id_proveedor');
            $this->db->where('nro_factura',$data['nro_factura']);
            $this->db->where('id_proveedor',$data['id_proveedor']);
            $result = $this->db->get('facturas');
            if($result->num_rows() == 1){
                return 0;
            }else{
                $query = $this->db->insert('facturas',$data);
                return $this->db->insert_id();
            }
        }

        //TOMA EL ID DE LA ULTIMA FACTURA Y EL $i DE ITERACION
        public function insumos_factura($last_id, $i){
            $catalogo = $this->input->post('catalogo');
            $cantidad = $this->input->post('cantidad');
            $peso_cantidad = $this->input->post('peso');

            $data = array(
                'id_factura' => $last_id,
                'id_catalogo' => $catalogo[$i],
                'cantidad' => $cantidad[$i],
                'peso_cantidad' => $peso_cantidad[$i]
            );

            return $this->db->insert('insumos_factura',$data);
        }

        //CREA ASIGNACION DESPUES DE MANDAR EL FORM

        public function crear_asignacion(){
            $data = array(
                'nro_asignacion' => $this->input->post('nro_asignacion'),
                'id_ca'=> $this->input->post('centro_a'),
                'id_status' => 3
            );
            $query = $this->db->insert('asignaciones_ca',$data);
            
            return $this->db->insert_id();
        }

        //Crea la Asignacion con los postdata que vienen de la vista asignacion_insumos
        public function insumos_asignacion($last_id,$i){ //$i viene del controlador admins para iterar los arreglos
            $catalogo = $this->input->post('catalogo');
            $cantidad = $this->input->post('cantidad');
            $peso_cantidad = $this->input->post('peso');

            $data = array(
                'id_asignacion' => $last_id,
                'id_catalogo' => $catalogo[$i],
                'cantidad' => $cantidad[$i],
                'peso_cantidad' => $peso_cantidad[$i]
            );
            return $this->db->insert('asignaciones_insumos',$data);
        }

        //Consulta Reportes para semaforo
        public function get_ultima_entrega(){
            $this->db->select('id_nota_entrega as id,fecha, nota_entrega, id_ca, id_fp');
            $query = $this->db->get('ultima_entrega');
            return $query->result_array();
        }
    }
?>
