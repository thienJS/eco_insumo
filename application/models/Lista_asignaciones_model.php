<?php
    class Lista_asignaciones_model extends CI_Model{
        //
        public function get_lista_asignaciones(){
            $this->db->select(' asignaciones_ca.id as id,
                                asignaciones_ca.nro_asignacion as nro_asignacion,
                                centros_acopio.name as name,
                                asignaciones_ca.fecha_asignacion as fecha_asignacion');
            $this->db->join('centros_acopio','asignaciones_ca.id_ca = centros_acopio.id','left');
            $query = $this->db->get('asignaciones_ca');
            return $query->result_array();
        }

        public function get_insumos_asignacion($postData){
            $this->db->select(' asignaciones_ca.nro_asignacion as nro_asignacion,
                                asignaciones_insumos.id_catalogo as id_catalogo,
                                insumos.name as name,
                                tipos_insumo.name as tipo_insumo,
								subtipos_insumo.name as subtipo_insumo,
								catalogo_insumos.presentacion as presentacion,
								asignaciones_insumos.cantidad as cantidad,
								asignaciones_insumos.peso_cantidad as peso'
                            );
            $this->db->join('asignaciones_ca','asignaciones_insumos.id_asignacion = asignaciones_ca.id','left');
            $this->db->join('catalogo_insumos','asignaciones_insumos.id_catalogo = catalogo_insumos.id','left');
            $this->db->join('insumos','catalogo_insumos.id_insumo = insumos.id','left');
			$this->db->join('tipos_insumo','insumos.id_tipo_insumo = tipos_insumo.id','left');
			$this->db->join('subtipos_insumo','insumos.id_subtipo_insumo = subtipos_insumo.id','left');
            $this->db->where('asignaciones_insumos.id_asignacion',$postData['this_asignacion']);
            $query = $this->db->get('asignaciones_insumos');
            return $query->result_array();
        }
    }
?>