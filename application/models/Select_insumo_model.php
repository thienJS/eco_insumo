<?php
    class Select_insumo_model extends CI_Model{

				//Consulta los Proveedores
		function get_proveedores(){
			$this->db->select('id,descripcion');
			$query = $this->db->get('proveedores');
			return $query->result_array();
		}

        // Consulta los tipos de insumos
        function get_tipos_insumo(){
            $response = array();
            $this->db->select('*');
            $q = $this->db->get('tipos_insumo');
            $response = $q->result_array();

            return $response;
        }

        // Consulta los subtipos de insumos
        function get_subtipos_insumo($postData){
            $response = array();

            $this->db->select('id,name');
            $this->db->where('id_tipo_insumo', $postData['this_tipo']);
            $q = $this->db->get('subtipos_insumo');
            $response = $q->result_array();

            return $response;
        }

        // Consulta los Insumos
        function get_insumos($postData){
            $response = array();

            $this->db->select('id,name');
			$this->db->where('id_subtipo_insumo', $postData['this_subtipo']);
            $this->db->order_by('name');
            $q = $this->db->get('insumos');
            $response = $q->result_array();

            return $response;
        }

        // Consulta las Presentaciones de Insumos
        public function get_catalogo($postData){
            $response = array();
            $this->db->select('id,presentacion,peso_unidad');
            $this->db->where('id_insumo', $postData['this_insumo']);
            $this->db->order_by('presentacion');
            $q = $this->db->get('catalogo_insumos');
            $response = $q->result_array();

            return $response;
        }

        //Consulta los Tipos de presentaciones de los Insumos
        public function get_tipo_presentacion(){
            $this->db->order_by('name');
            $query = $this->db->get('tipo_presentaciones');
            return $query->result_array();
        }

        //Consulta Inventario de DELAGRO
        public function get_inventario($postData){
            $this->db->select('cantidad as inventario');
            $this->db->where('id_catalogo',$postData['this_presentacion']);
            $q = $this->db->get('inventario');
            $response = $q->row_array();

            return $response;
        }

        public function get_inventario_ca($postData){
            $this->db->select('cantidad as inventario');
            $this->db->where('id_catalogo', $postData['this_presentacion']);
            $this->db->where('id_ca', $this->session->ca_id);
            $q = $this->db->get('inventario_ca');
            return $q->row_array();
        }
    }
?>
