<?php
  class Login_model extends CI_Model{
    // public function check_user(){
    //   $username = $this->input->post('username');
    //   $password = $this->input->post('password');

    //   $this->db->select('*');
    //   $this->db->where('username', $username);
    //   $this->db->where('password', $password);
    //   $q = $this->db->get('users');
    //   return $this->db->affected_rows();
    // }

    // public function login_user(){
    //   $username = $this->input->post('username');
    //   $password = $this->input->post('password');

    //   $this->db->select('*');
    //   $this->db->where('username', $username);
    //   $this->db->where('password', $password);
    //   $q = $this->db->get('users');
    //   $response = $q->result_array();
    //   return $response;
    // }

  //Login User
    public function login($username,$password){
      //Validate
      $this->db->select(' u.id_user as userid,
                          u.id_jefe_ca as jefeid,
                          ca.id as caid');
      $this->db->join('centros_acopio as ca', 'u.id_jefe_ca = ca.id_jefe_ca', 'left');
      $this->db->where('u.username',$username);
      $this->db->where('u.password',$password);
      $q = $this->db->get('users as u');
      if($q->num_rows() == 1){
          return $q->result_array();
      }else{
          return FALSE;
      }
    }
  }

?>