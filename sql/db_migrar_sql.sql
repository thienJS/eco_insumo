--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.12
-- Dumped by pg_dump version 9.5.12

-- Started on 2018-05-25 00:34:31

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2187 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 196 (class 1255 OID 25962)
-- Name: insertar_al_inventario(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.insertar_al_inventario() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
	INSERT INTO inventario(id_catalogo,cantidad,peso) 
	VALUES	(NEW.id,0,0);

	RETURN NEW;
END;
$$;


ALTER FUNCTION public.insertar_al_inventario() OWNER TO postgres;

--
-- TOC entry 195 (class 1255 OID 25960)
-- Name: sumar_al_inventario(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sumar_al_inventario() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE inventario 
	SET cantidad = cantidad + NEW.cantidad
	WHERE id_catalogo = NEW.id_catalogo;

	RETURN NEW;
END;
$$;


ALTER FUNCTION public.sumar_al_inventario() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 190 (class 1259 OID 25907)
-- Name: carga_insumos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.carga_insumos (
    id integer NOT NULL,
    nro_factura integer,
    id_catalogo integer,
    fecha_registro timestamp(0) without time zone DEFAULT now(),
    cantidad integer,
    peso_cantidad numeric(8,2)
);


ALTER TABLE public.carga_insumos OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 25905)
-- Name: carga_insumos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.carga_insumos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.carga_insumos_id_seq OWNER TO postgres;

--
-- TOC entry 2188 (class 0 OID 0)
-- Dependencies: 189
-- Name: carga_insumos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.carga_insumos_id_seq OWNED BY public.carga_insumos.id;


--
-- TOC entry 192 (class 1259 OID 25921)
-- Name: catalogo_insumos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.catalogo_insumos (
    id integer NOT NULL,
    id_tipo_insumo integer,
    id_subtipo_insumo integer,
    id_insumo integer,
    presentacion character varying(255),
    id_tipo_presentacion integer,
    peso_unidad numeric(8,2)
);


ALTER TABLE public.catalogo_insumos OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 25919)
-- Name: catalogo_insumos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.catalogo_insumos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalogo_insumos_id_seq OWNER TO postgres;

--
-- TOC entry 2189 (class 0 OID 0)
-- Dependencies: 191
-- Name: catalogo_insumos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.catalogo_insumos_id_seq OWNED BY public.catalogo_insumos.id;


--
-- TOC entry 181 (class 1259 OID 25814)
-- Name: insumos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.insumos (
    id integer NOT NULL,
    id_tipo_insumo integer,
    id_subtipo_insumo integer,
    id_presentacion integer,
    name character varying(255)
);


ALTER TABLE public.insumos OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 25817)
-- Name: insumos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.insumos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insumos_id_seq OWNER TO postgres;

--
-- TOC entry 2190 (class 0 OID 0)
-- Dependencies: 182
-- Name: insumos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.insumos_id_seq OWNED BY public.insumos.id;


--
-- TOC entry 194 (class 1259 OID 25949)
-- Name: inventario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inventario (
    id integer NOT NULL,
    id_catalogo integer,
    cantidad integer,
    peso numeric(8,2)
);


ALTER TABLE public.inventario OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 25947)
-- Name: inventario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.inventario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inventario_id_seq OWNER TO postgres;

--
-- TOC entry 2191 (class 0 OID 0)
-- Dependencies: 193
-- Name: inventario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.inventario_id_seq OWNED BY public.inventario.id;


--
-- TOC entry 183 (class 1259 OID 25824)
-- Name: subtipos_insumo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subtipos_insumo (
    id integer NOT NULL,
    id_tipo_insumo integer,
    name character varying(255)
);


ALTER TABLE public.subtipos_insumo OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 25827)
-- Name: subtipos_insumo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.subtipos_insumo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subtipos_insumo_id_seq OWNER TO postgres;

--
-- TOC entry 2192 (class 0 OID 0)
-- Dependencies: 184
-- Name: subtipos_insumo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.subtipos_insumo_id_seq OWNED BY public.subtipos_insumo.id;


--
-- TOC entry 185 (class 1259 OID 25829)
-- Name: tipo_presentaciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_presentaciones (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.tipo_presentaciones OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 25832)
-- Name: tipo_presentaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_presentaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_presentaciones_id_seq OWNER TO postgres;

--
-- TOC entry 2193 (class 0 OID 0)
-- Dependencies: 186
-- Name: tipo_presentaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_presentaciones_id_seq OWNED BY public.tipo_presentaciones.id;


--
-- TOC entry 187 (class 1259 OID 25834)
-- Name: tipos_insumo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipos_insumo (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.tipos_insumo OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 25837)
-- Name: tipos_insumo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipos_insumo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipos_insumo_id_seq OWNER TO postgres;

--
-- TOC entry 2194 (class 0 OID 0)
-- Dependencies: 188
-- Name: tipos_insumo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipos_insumo_id_seq OWNED BY public.tipos_insumo.id;


--
-- TOC entry 2023 (class 2604 OID 25910)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.carga_insumos ALTER COLUMN id SET DEFAULT nextval('public.carga_insumos_id_seq'::regclass);


--
-- TOC entry 2025 (class 2604 OID 25924)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogo_insumos ALTER COLUMN id SET DEFAULT nextval('public.catalogo_insumos_id_seq'::regclass);


--
-- TOC entry 2019 (class 2604 OID 25841)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumos ALTER COLUMN id SET DEFAULT nextval('public.insumos_id_seq'::regclass);


--
-- TOC entry 2026 (class 2604 OID 25952)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventario ALTER COLUMN id SET DEFAULT nextval('public.inventario_id_seq'::regclass);


--
-- TOC entry 2020 (class 2604 OID 25843)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subtipos_insumo ALTER COLUMN id SET DEFAULT nextval('public.subtipos_insumo_id_seq'::regclass);


--
-- TOC entry 2021 (class 2604 OID 25844)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_presentaciones ALTER COLUMN id SET DEFAULT nextval('public.tipo_presentaciones_id_seq'::regclass);


--
-- TOC entry 2022 (class 2604 OID 25845)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipos_insumo ALTER COLUMN id SET DEFAULT nextval('public.tipos_insumo_id_seq'::regclass);


--
-- TOC entry 2174 (class 0 OID 25907)
-- Dependencies: 190
-- Data for Name: carga_insumos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.carga_insumos (id, nro_factura, id_catalogo, fecha_registro, cantidad, peso_cantidad) FROM stdin;
1	231	1	2018-05-24 20:56:26	12	12.00
2	165	2	2018-05-25 00:21:01	159	159.00
\.


--
-- TOC entry 2195 (class 0 OID 0)
-- Dependencies: 189
-- Name: carga_insumos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.carga_insumos_id_seq', 2, true);


--
-- TOC entry 2176 (class 0 OID 25921)
-- Dependencies: 192
-- Data for Name: catalogo_insumos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.catalogo_insumos (id, id_tipo_insumo, id_subtipo_insumo, id_insumo, presentacion, id_tipo_presentacion, peso_unidad) FROM stdin;
1	1	1	1	Una presentacion	1	1.00
2	1	1	1	159	2	1.00
3	1	1	1	1	2	1.00
4	3	5	6	1594	2	12.00
5	3	5	7	Una Presentacion	2	1597.00
\.


--
-- TOC entry 2196 (class 0 OID 0)
-- Dependencies: 191
-- Name: catalogo_insumos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.catalogo_insumos_id_seq', 5, true);


--
-- TOC entry 2165 (class 0 OID 25814)
-- Dependencies: 181
-- Data for Name: insumos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.insumos (id, id_tipo_insumo, id_subtipo_insumo, id_presentacion, name) FROM stdin;
1	1	1	2	Insumo Agroquimico 1
2	1	2	1	Insumo Agroquimico 2
3	1	3	3	Insumo Agroquimico 3
4	2	4	3	Insumo Fertilizante 1
5	2	4	2	Insumo Fertilizante 2
6	3	5	2	Insumo Semilla 1
7	3	5	1	Insumo Semilla 2
\.


--
-- TOC entry 2197 (class 0 OID 0)
-- Dependencies: 182
-- Name: insumos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.insumos_id_seq', 7, true);


--
-- TOC entry 2178 (class 0 OID 25949)
-- Dependencies: 194
-- Data for Name: inventario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inventario (id, id_catalogo, cantidad, peso) FROM stdin;
1	1	12	0.00
3	3	0	0.00
4	4	0	0.00
5	5	0	0.00
2	2	159	0.00
\.


--
-- TOC entry 2198 (class 0 OID 0)
-- Dependencies: 193
-- Name: inventario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.inventario_id_seq', 5, true);


--
-- TOC entry 2167 (class 0 OID 25824)
-- Dependencies: 183
-- Data for Name: subtipos_insumo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subtipos_insumo (id, id_tipo_insumo, name) FROM stdin;
1	1	Agroquimico 1
2	1	Agroquimico 2
3	1	Agroquimico 3
4	2	Ferltilizantes
5	3	Semillas
\.


--
-- TOC entry 2199 (class 0 OID 0)
-- Dependencies: 184
-- Name: subtipos_insumo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.subtipos_insumo_id_seq', 5, true);


--
-- TOC entry 2169 (class 0 OID 25829)
-- Dependencies: 185
-- Data for Name: tipo_presentaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_presentaciones (id, name) FROM stdin;
1	KG
2	L
\.


--
-- TOC entry 2200 (class 0 OID 0)
-- Dependencies: 186
-- Name: tipo_presentaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_presentaciones_id_seq', 2, true);


--
-- TOC entry 2171 (class 0 OID 25834)
-- Dependencies: 187
-- Data for Name: tipos_insumo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipos_insumo (id, name) FROM stdin;
1	Agroquimicos
2	Fertilizantes
3	Semillas
\.


--
-- TOC entry 2201 (class 0 OID 0)
-- Dependencies: 188
-- Name: tipos_insumo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipos_insumo_id_seq', 3, true);


--
-- TOC entry 2036 (class 2606 OID 25913)
-- Name: carga_insumos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.carga_insumos
    ADD CONSTRAINT carga_insumos_pkey PRIMARY KEY (id);


--
-- TOC entry 2038 (class 2606 OID 25926)
-- Name: catalogo_insumos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogo_insumos
    ADD CONSTRAINT catalogo_insumos_pkey PRIMARY KEY (id);


--
-- TOC entry 2028 (class 2606 OID 25851)
-- Name: insumos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumos
    ADD CONSTRAINT insumos_pkey PRIMARY KEY (id);


--
-- TOC entry 2040 (class 2606 OID 25954)
-- Name: inventario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventario
    ADD CONSTRAINT inventario_pkey PRIMARY KEY (id);


--
-- TOC entry 2030 (class 2606 OID 25855)
-- Name: subtipos_insumo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subtipos_insumo
    ADD CONSTRAINT subtipos_insumo_pkey PRIMARY KEY (id);


--
-- TOC entry 2032 (class 2606 OID 25857)
-- Name: tipo_presentaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_presentaciones
    ADD CONSTRAINT tipo_presentaciones_pkey PRIMARY KEY (id);


--
-- TOC entry 2034 (class 2606 OID 25859)
-- Name: tipos_insumo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipos_insumo
    ADD CONSTRAINT tipos_insumo_pkey PRIMARY KEY (id);


--
-- TOC entry 2049 (class 2620 OID 25961)
-- Name: after_carga_insumo; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_carga_insumo AFTER INSERT ON public.carga_insumos FOR EACH ROW EXECUTE PROCEDURE public.sumar_al_inventario();


--
-- TOC entry 2050 (class 2620 OID 25963)
-- Name: after_catalogo_insumo; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_catalogo_insumo AFTER INSERT ON public.catalogo_insumos FOR EACH ROW EXECUTE PROCEDURE public.insertar_al_inventario();


--
-- TOC entry 2044 (class 2606 OID 25927)
-- Name: catalogo_insumos_id_insumo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogo_insumos
    ADD CONSTRAINT catalogo_insumos_id_insumo_fkey FOREIGN KEY (id_insumo) REFERENCES public.insumos(id);


--
-- TOC entry 2045 (class 2606 OID 25932)
-- Name: catalogo_insumos_id_subtipo_insumo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogo_insumos
    ADD CONSTRAINT catalogo_insumos_id_subtipo_insumo_fkey FOREIGN KEY (id_subtipo_insumo) REFERENCES public.subtipos_insumo(id);


--
-- TOC entry 2046 (class 2606 OID 25937)
-- Name: catalogo_insumos_id_tipo_insumo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogo_insumos
    ADD CONSTRAINT catalogo_insumos_id_tipo_insumo_fkey FOREIGN KEY (id_tipo_insumo) REFERENCES public.tipos_insumo(id);


--
-- TOC entry 2047 (class 2606 OID 25942)
-- Name: catalogo_insumos_id_tipo_presentacion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogo_insumos
    ADD CONSTRAINT catalogo_insumos_id_tipo_presentacion_fkey FOREIGN KEY (id_tipo_presentacion) REFERENCES public.tipo_presentaciones(id);


--
-- TOC entry 2041 (class 2606 OID 25885)
-- Name: insumos_id_subtipo_insumo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumos
    ADD CONSTRAINT insumos_id_subtipo_insumo_fkey FOREIGN KEY (id_subtipo_insumo) REFERENCES public.subtipos_insumo(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2042 (class 2606 OID 25890)
-- Name: insumos_id_tipo_insumo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumos
    ADD CONSTRAINT insumos_id_tipo_insumo_fkey FOREIGN KEY (id_tipo_insumo) REFERENCES public.tipos_insumo(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2048 (class 2606 OID 25955)
-- Name: inventario_id_catalogo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventario
    ADD CONSTRAINT inventario_id_catalogo_fkey FOREIGN KEY (id_catalogo) REFERENCES public.catalogo_insumos(id);


--
-- TOC entry 2043 (class 2606 OID 25900)
-- Name: subtipos_insumo_id_tipo_insumo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subtipos_insumo
    ADD CONSTRAINT subtipos_insumo_id_tipo_insumo_fkey FOREIGN KEY (id_tipo_insumo) REFERENCES public.tipos_insumo(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2186 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-05-25 00:34:31

--
-- PostgreSQL database dump complete
--

