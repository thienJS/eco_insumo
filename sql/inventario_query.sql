﻿SELECT 	inventario.id_catalogo,
	tipos_insumo.name as tipo_insumo,
	subtipos_insumo.name as subtipo_insumo,
	insumos.name as nombre_insumo,
	catalogo_insumos.presentacion,
	tipo_presentaciones.name as tipo_presentacion,
	cantidad,
	peso
FROM inventario
LEFT JOIN catalogo_insumos ON catalogo_insumos.id = inventario.id_catalogo
LEFT JOIN insumos ON insumos.id  = catalogo_insumos.id_insumo
LEFT JOIN tipos_insumo ON tipos_insumo.id = catalogo_insumos.id_tipo_insumo
LEFT JOIN subtipos_insumo ON subtipos_insumo.id = catalogo_insumos.id_subtipo_insumo
LEFT JOIN tipo_presentaciones ON tipo_presentaciones.id = catalogo_insumos.id_tipo_presentacion
