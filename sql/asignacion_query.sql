﻿DROP TABLE asignacion_ca;
CREATE TABLE asignacion_ca(
	id SERIAL,
	nro_asignacion SERIAL,
	id_catalogo INT,
	id_ca INT,
	cantidad INT,
	peso NUMERIC(8,2),
	fecha_asignacion TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	FOREIGN KEY (id_catalogo) REFERENCES catalogo_insumos(id),
	FOREIGN KEY (id_ca) REFERENCES centros_acopio(id)
	
);